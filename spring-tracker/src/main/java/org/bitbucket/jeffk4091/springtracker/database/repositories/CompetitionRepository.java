package org.bitbucket.jeffk4091.springtracker.database.repositories;

import java.util.List;

import org.bitbucket.jeffk4091.springtracker.database.entities.Competition;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompetitionRepository extends CrudRepository<Competition, Long> {

	List<Competition> findByStatus(int status);

	//Index, Currently running
	List<Competition> findByStatusInAndPrivacyInOrderByCompidDesc(List<Integer> status, List<Long> privacy);

	//Index, Upcoming
	List<Competition> findByStatusAndPrivacyInOrderByStarttimeAsc(int status, List<Long> privacy);

	//Index, Ended
	List<Competition> findTop10ByStatusAndPrivacyInOrderByEndtimeDesc(int status, List<Long> privacy);

	List<Competition> findByStatusAndPrivacyInOrderByEndtimeDesc(int status, List<Long> privacy);

}
