package org.bitbucket.jeffk4091.springtracker.json;

public class CompetitionDetails {

	private final long compId;
	private final String compName;
	private final String skill;
	private final int status;
	private final String startTime;
	private final String endTime;
	private final String updateTime;
	private final long privacy;

	public CompetitionDetails(long compId, String compName, String skill, int status, String startTime, String endTime,
			String updateTime, long privacy) {
		this.compId = compId;
		this.compName = compName;
		this.skill = skill;
		this.status = status;
		this.startTime = startTime;
		this.endTime = endTime;
		this.updateTime = updateTime;
		this.privacy = privacy;
	}

	public long getCompId() {
		return compId;
	}

	public String getCompName() {
		return compName;
	}

	public String getSkill() {
		return skill;
	}

	public int getStatus() {
		return status;
	}

	public String getStartTime() {
		return startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public long getPrivacy() {
		return privacy;
	}
}
