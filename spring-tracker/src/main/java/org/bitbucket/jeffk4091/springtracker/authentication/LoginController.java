package org.bitbucket.jeffk4091.springtracker.authentication;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/login")
public class LoginController {

	@Autowired
	UUIDAuthenticationService authenticationService;

	@PostMapping("/")
	public String login(@RequestParam("password") String password) {
		return authenticationService.login(password).orElseThrow(() -> new RuntimeException("Invalid login"));
	}
}
