package org.bitbucket.jeffk4091.springtracker.update;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Executors;

import org.bitbucket.jeffk4091.rshiscoresfetcher.hiscoresfetcher.RsHiscoresDownloader;
import org.bitbucket.jeffk4091.rshiscoresfetcher.skills.Rs3Skill;
import org.bitbucket.jeffk4091.rshiscoresfetcher.skills.Skill;
import org.bitbucket.jeffk4091.rshiscoresfetcher.util.HttpClient;
import org.bitbucket.jeffk4091.springtracker.database.CompetitionStatus;
import org.bitbucket.jeffk4091.springtracker.database.entities.Competition;
import org.bitbucket.jeffk4091.springtracker.database.entities.Participant;
import org.bitbucket.jeffk4091.springtracker.database.repositories.CompetitionRepository;
import org.bitbucket.jeffk4091.springtracker.database.repositories.ParticipantRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Service;

@Service
public class EndCompService extends StatTaskScheduler
		implements StatTaskListener, ApplicationListener<ApplicationReadyEvent> {

	@Autowired
	private CompetitionRepository competitionRepository;

	@Autowired
	private ParticipantRepository participantRepository;

	private final Logger logger = LoggerFactory.getLogger(EndCompService.class);

	public EndCompService() {
		super(Executors.newScheduledThreadPool(4));
	}

	public boolean registerComp(long compid) {
		List<Participant> participants = this.participantRepository.findByCompid(compid);
		Optional<Competition> comp = this.competitionRepository.findById(compid);
		if (participants.size() != 0 && comp.isPresent()) {
			initializeTasks(comp.get());
			return true;
		}
		return false;
	}

	public boolean rescheduleComp(long compid) {
		Optional<Competition> comp = this.competitionRepository.findById(compid);
		if (comp.isPresent()) {
			if (this.cancelTask(compid)) {
				initializeTasks(comp.get());
				return true;
			}
			return false;
		}
		return false;
	}

	@Override
	public void updateCompleted(long compid) {
		logger.info("Competition [" + compid + "] has ended, updating status to finished.");
		updateCompetitionState(compid, CompetitionStatus.FINISHED.getStatus());
		removeCompletedTask(compid);
	}

	private void updateCompetitionState(long compid, int status) {
		Optional<Competition> comp = this.competitionRepository.findById(compid);
		comp.ifPresent(competition -> {
			competition.setStatus(status);
			competition.setUpdatetime(Instant.now().getEpochSecond());
			this.competitionRepository.save(competition);
		});
	}

	@Override
	public void updateStarted(long compid) {
		logger.info("Competition [" + compid + "] is ending, performing final update.");
		updateCompetitionState(compid, CompetitionStatus.UPDATING.getStatus());
	}

	@Override
	public void onApplicationEvent(ApplicationReadyEvent event) {
		logger.info("EndCompService creating end tasks for competitions in a NOT_STARTED state.");
		List<Competition> notStartedCompetitions = this.competitionRepository
				.findByStatus(CompetitionStatus.NOT_STARTED.getStatus());
		for (Competition comp : notStartedCompetitions) {
			if (!(Instant.now().getEpochSecond() - comp.getEndtime() > 0)) {
				initializeTasks(comp);
			} else {
				logger.warn("Competition with comp id [" + comp.getCompid()
						+ "] was supposed to start and end prior to now, not registering an end task.");
			}
		}
		/*
		 * Design Decision - set an end time for ANY competition in started status, even if its
		 * already ended.  We will allow one more final stat update for that.
		 */
		logger.info("EndCompService creating end tasks for competitions in a STARTED state.");
		List<Competition> startedCompetitions = this.competitionRepository
				.findByStatus(CompetitionStatus.STARTED.getStatus());
		for (Competition comp : startedCompetitions) {
			initializeTasks(comp);
		}
		//Also fix and schedule orphaned competitions that were updating when service went off
		logger.info("EndCompService fixing & creating end tasks for competitions in an UPDATING state.");
		List<Competition> updatingCompetitions = this.competitionRepository
				.findByStatus(CompetitionStatus.UPDATING.getStatus());
		for (Competition comp : updatingCompetitions) {
			comp.setStatus(CompetitionStatus.STARTED.getStatus());
			Competition updatedComp = this.competitionRepository.save(comp);
			initializeTasks(updatedComp);
		}
	}

	private void initializeTasks(Competition comp) {
		Skill skill = Rs3Skill.valueOf(comp.getSkill().toUpperCase()).getSkill();
		UpdateTask task = new UpdateTask(this.participantRepository, this, skill, comp.getCompid(),
				new RsHiscoresDownloader(HttpClient.HISCORES_URL));
		long delay = calculateDelay(comp);
		logger.debug("Registering end task for comp id [" + comp.getCompid() + "] with delay of " + delay + " seconds");
		this.scheduleTask(comp.getCompid(), task, delay);
	}

	long calculateDelay(Competition comp) {
		return comp.getEndtime() - Instant.now().getEpochSecond();
	}
}
