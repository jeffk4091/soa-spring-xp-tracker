package org.bitbucket.jeffk4091.springtracker.authentication;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/settings")
public class SettingsController {

	@Autowired
	UUIDAuthenticationService authenticationService;

	@GetMapping("/logout")
	public boolean logout(@AuthenticationPrincipal final User user) {
		authenticationService.logout(user);
		return true;
	}
}
