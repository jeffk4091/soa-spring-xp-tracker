package org.bitbucket.jeffk4091.springtracker.index;

import java.util.List;

import javax.servlet.ServletRequest;

import org.bitbucket.jeffk4091.springtracker.json.CompetitionDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/index")
public class IndexController {

	@Autowired
	private IndexService indexService;

	@GetMapping("/active")
	public List<CompetitionDetails> getActiveCompetitions() {
		return this.indexService.getActiveCompetitions(true);
	}

	@GetMapping("/upcoming")
	public List<CompetitionDetails> getUpcomingCompetitions() {
		return this.indexService.getUpcomingCompetitions(true);
	}

	@GetMapping("/previous")
	public List<CompetitionDetails> getPreviousCompetitions(ServletRequest request) {
		if (!request.getParameterMap().containsKey("viewall")) {
			return this.indexService.getPreviousCompetitions(true, false);
		} else {
			return this.indexService.getPreviousCompetitions(true, true);
		}
	}
}
