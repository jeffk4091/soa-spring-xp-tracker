package org.bitbucket.jeffk4091.springtracker.update;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class StatTaskScheduler {

	private final Map<Long, ScheduledFuture<?>> taskMap = new ConcurrentHashMap<>();
	private ScheduledExecutorService executorService;

	public StatTaskScheduler(ScheduledExecutorService executorService) {
		this.executorService = executorService;
	}

	public void scheduleTask(long compid, StatTask statTask, long delay) {
		ScheduledFuture<?> task = this.executorService.schedule(statTask, delay, TimeUnit.SECONDS);
		this.taskMap.put(compid, task);
	}

	public boolean cancelTask(long compid) {
		if (this.taskMap.containsKey(compid)) {
			ScheduledFuture<?> future = this.taskMap.get(compid);
			if (future.cancel(false)) {
				this.taskMap.remove(compid);
				return true;
			}
		}
		return false;
	}

	protected void removeCompletedTask(long compid) {
		this.taskMap.remove(compid);
	}

	public boolean isCompScheduled(long compid) {
		return this.taskMap.containsKey(compid);
	}

	ScheduledFuture<?> getFutureForCompid(long compid) {
		return this.taskMap.get(compid);
	}

	void setExecutorService(ScheduledExecutorService executorService) {
		this.executorService = executorService;
	}
}
