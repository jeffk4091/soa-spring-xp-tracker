package org.bitbucket.jeffk4091.springtracker.database.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "settings")
public class Setting {

	@Id
	@Column(columnDefinition = "varchar(30)", nullable = false)
	private String setname;

	private String setval;

	public String getSetname() {
		return setname;
	}

	public void setSetname(String setname) {
		this.setname = setname;
	}

	public String getSetval() {
		return setval;
	}

	public void setSetval(String setval) {
		this.setval = setval;
	}
}
