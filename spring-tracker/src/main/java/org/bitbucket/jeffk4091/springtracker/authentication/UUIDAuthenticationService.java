package org.bitbucket.jeffk4091.springtracker.authentication;

import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.bitbucket.jeffk4091.springtracker.database.repositories.SettingsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UUIDAuthenticationService implements UserAuthenticationService {

	@Autowired
	private SettingsRepository repository;
	private BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(BCryptPasswordEncoder.BCryptVersion.$2Y, 12);
	private ConcurrentHashMap<String, User> loggedInUsersMap = new ConcurrentHashMap<>();
	private static final String PASS_SETTING = "adminpass";
	private static final int INACTIVITY_TIMER = 25; // 25 minutes
	private Logger logger = LoggerFactory.getLogger(UUIDAuthenticationService.class);

	@Override
	public Optional<String> login(String password) {
		String pass = repository.findBySetname(PASS_SETTING).getSetval();

		if (encoder.matches(password, pass)) {
			String uuid = UUID.randomUUID().toString();
			logger.info("User login was successful, generated uuid [" + uuid + "]");
			User user = new User(pass, uuid);
			user.setLastActive(Instant.now());
			loggedInUsersMap.put(uuid, user);
			return Optional.of(uuid);
		} else {
			logger.info("A user attempted to sign in but their credentials were incorrect.");
			return Optional.empty();
		}
	}

	@Override
	public Optional<User> findByToken(String token) {
		Optional<User> foundUser = Optional.ofNullable(loggedInUsersMap.get(token));
		foundUser.ifPresent(user -> user.setLastActive(Instant.now()));
		return foundUser;
	}

	@Override
	public void logout(User user) {
		loggedInUsersMap.values().stream().filter(user1 -> user1.getUuid().equals(user.getUuid()))
				.forEach(user1 -> loggedInUsersMap.remove(user1.getUuid()));
		logger.info(
				"User with uuid [" + user.getUuid() + "] logged out, current # of users logged in: " + loggedInUsersMap
						.size());
	}

	//Terminate sessions; a task will run this, per Josh set this to terminate a session after 25 mins of inactivity.
	public void terminateOldSessions() {
		Instant now = Instant.now();
		List<User> usersToRemove = loggedInUsersMap.values().stream()
				.filter(user -> Duration.between(user.getLastActive(), now).getSeconds() > (60 * INACTIVITY_TIMER))
				.collect(Collectors.toList());
		for (User user : usersToRemove) {
			logger.info("Terminating session for uuid " + user.getUuid() + " due to inactivity.");
			loggedInUsersMap.remove(user.getUuid());
		}
	}
}
