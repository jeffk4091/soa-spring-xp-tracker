/**
 * Classes used to contain JSON output for REST Api.
 * <p>
 * Classes within this package are expected to be immutable.
 * </p>
 */
package org.bitbucket.jeffk4091.springtracker.json;