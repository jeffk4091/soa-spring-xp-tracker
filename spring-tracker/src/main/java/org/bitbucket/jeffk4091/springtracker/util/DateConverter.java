package org.bitbucket.jeffk4091.springtracker.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.TimeZone;

public class DateConverter {

	private String format = "yyyy/MM/dd HH:mm";

	public String formatEpochToString(long epochTime) {
		DateTimeFormatter df = DateTimeFormatter.ofPattern(format).withZone(ZoneId.of("UTC"));
		return df.format(Instant.ofEpochSecond(epochTime));
	}

	public long formatDateToEpochSecond(String date) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		Instant instant = sdf.parse(date).toInstant();
		return instant.getEpochSecond();
	}
}
