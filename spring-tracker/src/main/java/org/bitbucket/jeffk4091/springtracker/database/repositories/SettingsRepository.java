package org.bitbucket.jeffk4091.springtracker.database.repositories;

import org.bitbucket.jeffk4091.springtracker.database.entities.Setting;
import org.springframework.data.repository.CrudRepository;

public interface SettingsRepository extends CrudRepository<Setting, String> {

	Setting findBySetname(String setName);

}
