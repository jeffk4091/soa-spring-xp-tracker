package org.bitbucket.jeffk4091.springtracker.authentication;

import java.time.Instant;
import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class User implements UserDetails {

	private String password;
	private String uuid;
	private Instant lastActive;

	public User(String password, String uuid) {
		this.password = password;
		this.uuid = uuid;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return null;
	}

	@Override
	public String getPassword() {
		return this.password;
	}

	public String getUuid() {
		return this.uuid;
	}

	@Override
	public String getUsername() {
		return null;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	public Instant getLastActive() {
		return lastActive;
	}

	public void setLastActive(Instant instant) {
		this.lastActive = instant;
	}
}
