package org.bitbucket.jeffk4091.springtracker.database;

public enum Privacy {
	NOT_PRIVATE(0L), PRIVATE(1L);

	private Long privacy;

	Privacy(long privacy) {
		this.privacy = privacy;
	}

	public Long getPrivacy() {
		return privacy;
	}
}
