package org.bitbucket.jeffk4091.springtracker.update;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import org.bitbucket.jeffk4091.rshiscoresfetcher.hiscoresfetcher.HiscoresRecord;
import org.bitbucket.jeffk4091.rshiscoresfetcher.hiscoresfetcher.RsHiscoresDownloader;
import org.bitbucket.jeffk4091.rshiscoresfetcher.hiscoresfetcher.RsHiscoresDownloaderListener;
import org.bitbucket.jeffk4091.rshiscoresfetcher.skills.Skill;
import org.bitbucket.jeffk4091.springtracker.database.entities.Participant;
import org.bitbucket.jeffk4091.springtracker.database.repositories.ParticipantRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class StatTask implements Runnable, RsHiscoresDownloaderListener {
	private final ParticipantRepository participantRepository;
	private final StatTaskListener listener;
	private final List<Skill> skill = new ArrayList<>();
	private final long compid;
	private final Logger logger = LoggerFactory.getLogger(StatTask.class);
	private final RsHiscoresDownloader downloader;
	//Submit database update tasks on their own thread and continue fetching data while that runs
	private final ExecutorService service = Executors.newCachedThreadPool();

	public StatTask(ParticipantRepository participantRepository, StatTaskListener listener, Skill skill, long compid,
			RsHiscoresDownloader downloader) {
		this.participantRepository = participantRepository;
		this.listener = listener;
		this.skill.add(skill);
		this.compid = compid;
		this.downloader = downloader;
		this.downloader.addListener(this);
	}

	@Override
	public void run() {
		List<Participant> participants = this.participantRepository.findByCompid(compid);
		List<String> names = participants.stream().map(Participant::getPlayer).collect(Collectors.toList());
		listener.updateStarted(compid);
		Instant start = Instant.now();
		logger.info(
				"Beginning competition update for Competition ID [" + compid + "], number of players: " + participants
						.size() + ", skill: " + skill.get(0).getSkillName());
		downloader.listenForStatsOfPlayers(names, skill);
		//After finished
		Instant end = Instant.now();
		logger.info("Competition update for Competition ID [" + compid + "] completed, taking " + Duration
				.between(start, end).getSeconds() + " seconds. Updating state.");
		listener.updateCompleted(compid);
		service.shutdown();
	}

	@Override
	public void notifySuccessfulFetch(List<HiscoresRecord> record) {
		service.submit(() -> updatePlayer(record));
	}

	@Override
	public void notifyFailedFetch(String name) {
		logger.warn("Failed to retrieve stats for Player [" + name + "], Competition ID [" + compid + "]");
		service.submit(() -> updateFailedPlayer(name));
	}

	public ParticipantRepository getParticipantRepository() {
		return participantRepository;
	}

	public List<Skill> getSkill() {
		return skill;
	}

	public long getCompid() {
		return compid;
	}

	abstract void updatePlayer(List<HiscoresRecord> record);

	abstract void updateFailedPlayer(String name);

}
