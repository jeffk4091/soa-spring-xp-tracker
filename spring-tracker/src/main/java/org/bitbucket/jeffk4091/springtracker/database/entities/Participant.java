package org.bitbucket.jeffk4091.springtracker.database.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "participants")
public class Participant {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(columnDefinition = "INT(10) UNSIGNED", nullable = false)
	private long id;

	@Column(columnDefinition = "varchar(12)", nullable = false)
	private String player;

	@Column(columnDefinition = "INT(10) UNSIGNED", nullable = false)
	private long compid;

	@Column(columnDefinition = "bigint(20) unsigned", nullable = false)
	private long startxp;

	@Column(columnDefinition = "INT(10) UNSIGNED", nullable = false)
	private int startlvl;

	@Column(columnDefinition = "bigint(20) unsigned", nullable = false)
	private long endxp;

	@Column(columnDefinition = "INT(10) UNSIGNED", nullable = false)
	private int endlvl;

	@Column(columnDefinition = "bigint(20) unsigned", nullable = false)
	private long xpgained;

	@Column(columnDefinition = "INT(10) UNSIGNED", nullable = false)
	private int lvlgained;

	public long getId() {
		return id;
	}

	public String getPlayer() {
		return player;
	}

	public void setPlayer(String player) {
		this.player = player;
	}

	public long getCompid() {
		return compid;
	}

	public void setCompid(int compid) {
		this.compid = compid;
	}

	public long getStartxp() {
		return startxp;
	}

	public void setStartxp(long startxp) {
		this.startxp = startxp;
	}

	public int getStartlvl() {
		return startlvl;
	}

	public void setStartlvl(int startlvl) {
		this.startlvl = startlvl;
	}

	public long getEndxp() {
		return endxp;
	}

	public void setEndxp(long endxp) {
		this.endxp = endxp;
	}

	public int getEndlvl() {
		return endlvl;
	}

	public void setEndlvl(int endlvl) {
		this.endlvl = endlvl;
	}

	public long getXpgained() {
		return xpgained;
	}

	public void setXpgained(long xpgained) {
		this.xpgained = xpgained;
	}

	public int getLvlgained() {
		return lvlgained;
	}

	public void setLvlgained(int lvlgained) {
		this.lvlgained = lvlgained;
	}

	public void initializeNewPlayer(String player, int compid) {
		this.player = player;
		this.compid = compid;
		this.startlvl = 1;
		this.startxp = 0;
		this.endlvl = 1;
		this.endxp = 0;
		this.xpgained = 0;
		this.lvlgained = 0;
	}

	public void calculateXpAndLvlGain() {
		this.lvlgained = getEndlvl() - getStartlvl();
		this.xpgained = getEndxp() - getStartxp();
	}
}
