package org.bitbucket.jeffk4091.springtracker.database.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "competitions")
public class Competition {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(columnDefinition = "INT(10) UNSIGNED", nullable = false)
	private long compid;

	@Column(columnDefinition = "varchar(50)", nullable = false)
	private String compname;

	@Column(columnDefinition = "varchar(30)", nullable = false)
	private String skill;

	@Column(nullable = false)
	private int status;

	@Column(columnDefinition = "INT(10) UNSIGNED", nullable = false)
	private long starttime;

	@Column(columnDefinition = "INT(10) UNSIGNED", nullable = false)
	private long endtime;

	@Column(columnDefinition = "INT(10) UNSIGNED", nullable = false)
	private long updatetime;

	@Column(columnDefinition = "INT(10) UNSIGNED", nullable = false)
	private long privacy;

	public long getCompid() {
		return compid;
	}

	public String getCompname() {
		return compname;
	}

	public void setCompname(String compname) {
		this.compname = compname;
	}

	public String getSkill() {
		return skill;
	}

	public void setSkill(String skill) {
		this.skill = skill;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public long getStarttime() {
		return starttime;
	}

	public void setStarttime(long starttime) {
		this.starttime = starttime;
	}

	public long getEndtime() {
		return endtime;
	}

	public void setEndtime(long endtime) {
		this.endtime = endtime;
	}

	public long getUpdatetime() {
		return updatetime;
	}

	public void setUpdatetime(long updatetime) {
		this.updatetime = updatetime;
	}

	public long getPrivacy() {
		return privacy;
	}

	public void setPrivacy(long privacy) {
		this.privacy = privacy;
	}
}
