package org.bitbucket.jeffk4091.springtracker.authentication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class TerminateInactiveSessionsTask {

	@Autowired
	private UUIDAuthenticationService authenticationService;

	private Logger logger = LoggerFactory.getLogger(TerminateInactiveSessionsTask.class);

	@Async
	@Scheduled(cron = "0 * * * * *")
	public void runTerminateInactiveSessionsTask() {
		logger.trace("Executing terminate inactive sessions task");
		authenticationService.terminateOldSessions();
		logger.trace("Terminate Inactive Sessions Task completed");
	}
}
