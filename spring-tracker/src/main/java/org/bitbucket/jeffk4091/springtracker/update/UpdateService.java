package org.bitbucket.jeffk4091.springtracker.update;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.bitbucket.jeffk4091.rshiscoresfetcher.hiscoresfetcher.RsHiscoresDownloader;
import org.bitbucket.jeffk4091.rshiscoresfetcher.skills.Rs3Skill;
import org.bitbucket.jeffk4091.rshiscoresfetcher.skills.Skill;
import org.bitbucket.jeffk4091.rshiscoresfetcher.util.HttpClient;
import org.bitbucket.jeffk4091.springtracker.database.CompetitionStatus;
import org.bitbucket.jeffk4091.springtracker.database.entities.Competition;
import org.bitbucket.jeffk4091.springtracker.database.entities.Participant;
import org.bitbucket.jeffk4091.springtracker.database.repositories.CompetitionRepository;
import org.bitbucket.jeffk4091.springtracker.database.repositories.ParticipantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UpdateService implements StatTaskListener {

	@Autowired
	private CompetitionRepository competitionRepository;

	@Autowired
	private ParticipantRepository participantRepository;

	ExecutorService executorService = Executors.newCachedThreadPool();

	public boolean updateStatsForCompetition(long compid) {
		List<Participant> participants = this.participantRepository.findByCompid(compid);
		Optional<Competition> comp = this.competitionRepository.findById(compid);
		if (participants.size() != 0 && comp.isPresent()) {
			if(comp.get().getStatus() == CompetitionStatus.STARTED.getStatus() && !(Instant.now().getEpochSecond() - comp.get().getEndtime() > 0)) {
				Skill skill = Rs3Skill.valueOf(comp.get().getSkill().toUpperCase()).getSkill();
				UpdateTask task = new UpdateTask(this.participantRepository, this, skill, compid, new RsHiscoresDownloader(HttpClient.HISCORES_URL));
				this.executorService.submit(task);
				return true;
			}
		}
		return false;
	}

	void updateCompetitionState(long compid, CompetitionStatus status) {
		Optional<Competition> comp = this.competitionRepository.findById(compid);
		comp.ifPresent(competition -> {
			competition.setStatus(status.getStatus());
			competition.setUpdatetime(Instant.now().getEpochSecond());
			this.competitionRepository.save(competition);
		});
	}

	@Override
	public void updateCompleted(long compid) {
		updateCompetitionState(compid, CompetitionStatus.STARTED);
	}

	@Override
	public void updateStarted(long compid) {
		updateCompetitionState(compid, CompetitionStatus.UPDATING);
	}

	//For test purposes only
	void setExecutorService(ExecutorService executorService) {
		this.executorService = executorService;
	}
}
