package org.bitbucket.jeffk4091.springtracker.update;

public interface StatTaskListener {
	void updateCompleted(long compid);

	void updateStarted(long compid);
}
