package org.bitbucket.jeffk4091.springtracker.index;

import java.util.ArrayList;
import java.util.List;

import org.bitbucket.jeffk4091.springtracker.database.CompetitionStatus;
import org.bitbucket.jeffk4091.springtracker.database.Privacy;
import org.bitbucket.jeffk4091.springtracker.database.entities.Competition;
import org.bitbucket.jeffk4091.springtracker.database.repositories.CompetitionRepository;
import org.bitbucket.jeffk4091.springtracker.json.CompetitionDetails;
import org.bitbucket.jeffk4091.springtracker.util.DateConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IndexService {

	@Autowired
	private CompetitionRepository competitionRepository;

	public List<CompetitionDetails> getActiveCompetitions(boolean isPrivate) {
		List<CompetitionDetails> detailsList = new ArrayList<>();
		List<Long> privacyStatus = new ArrayList<>();
		privacyStatus.add(Privacy.NOT_PRIVATE.getPrivacy());
		if (isPrivate)
			privacyStatus.add(Privacy.PRIVATE.getPrivacy());
		List<Integer> status = new ArrayList<>();
		status.add(CompetitionStatus.STARTED.getStatus());
		status.add(CompetitionStatus.UPDATING.getStatus());
		DateConverter converter = new DateConverter();
		for (Competition competition : competitionRepository
				.findByStatusInAndPrivacyInOrderByCompidDesc(status, privacyStatus)) {
			detailsList.add(new CompetitionDetails(competition.getCompid(), competition.getCompname(),
					competition.getSkill(), competition.getStatus(),
					converter.formatEpochToString(competition.getStarttime()),
					converter.formatEpochToString(competition.getEndtime()),
					converter.formatEpochToString(competition.getUpdatetime()), competition.getPrivacy()));

		}
		return detailsList;
	}

	public List<CompetitionDetails> getUpcomingCompetitions(boolean isPrivate) {
		List<CompetitionDetails> detailsList = new ArrayList<>();
		List<Long> privacyStatus = new ArrayList<>();
		privacyStatus.add(Privacy.NOT_PRIVATE.getPrivacy());
		if (isPrivate)
			privacyStatus.add(Privacy.PRIVATE.getPrivacy());
		DateConverter converter = new DateConverter();
		for (Competition competition : competitionRepository
				.findByStatusAndPrivacyInOrderByStarttimeAsc(CompetitionStatus.NOT_STARTED.getStatus(),
						privacyStatus)) {
			detailsList.add(new CompetitionDetails(competition.getCompid(), competition.getCompname(),
					competition.getSkill(), competition.getStatus(),
					converter.formatEpochToString(competition.getStarttime()),
					converter.formatEpochToString(competition.getEndtime()),
					converter.formatEpochToString(competition.getUpdatetime()), competition.getPrivacy()));

		}
		return detailsList;
	}

	public List<CompetitionDetails> getPreviousCompetitions(boolean isPrivate, boolean getAll) {
		List<CompetitionDetails> detailsList = new ArrayList<>();
		List<Long> privacyStatus = new ArrayList<>();
		privacyStatus.add(Privacy.NOT_PRIVATE.getPrivacy());
		if (isPrivate)
			privacyStatus.add(Privacy.PRIVATE.getPrivacy());
		DateConverter converter = new DateConverter();
		List<Competition> comps;
		if (!getAll) {
			comps = competitionRepository
					.findTop10ByStatusAndPrivacyInOrderByEndtimeDesc(CompetitionStatus.FINISHED.getStatus(),
							privacyStatus);
		} else {
			comps = competitionRepository
					.findByStatusAndPrivacyInOrderByEndtimeDesc(CompetitionStatus.FINISHED.getStatus(), privacyStatus);
		}
		for (Competition competition : comps) {
			detailsList.add(new CompetitionDetails(competition.getCompid(), competition.getCompname(),
					competition.getSkill(), competition.getStatus(),
					converter.formatEpochToString(competition.getStarttime()),
					converter.formatEpochToString(competition.getEndtime()),
					converter.formatEpochToString(competition.getUpdatetime()), competition.getPrivacy()));

		}
		return detailsList;
	}
}
