package org.bitbucket.jeffk4091.springtracker.update;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Executors;

import org.bitbucket.jeffk4091.rshiscoresfetcher.hiscoresfetcher.RsHiscoresDownloader;
import org.bitbucket.jeffk4091.rshiscoresfetcher.skills.Rs3Skill;
import org.bitbucket.jeffk4091.rshiscoresfetcher.skills.Skill;
import org.bitbucket.jeffk4091.rshiscoresfetcher.util.HttpClient;
import org.bitbucket.jeffk4091.springtracker.database.CompetitionStatus;
import org.bitbucket.jeffk4091.springtracker.database.entities.Competition;
import org.bitbucket.jeffk4091.springtracker.database.entities.Participant;
import org.bitbucket.jeffk4091.springtracker.database.repositories.CompetitionRepository;
import org.bitbucket.jeffk4091.springtracker.database.repositories.ParticipantRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Service;

@Service
public class StartCompService extends StatTaskScheduler
		implements StatTaskListener, ApplicationListener<ApplicationReadyEvent> {

	@Autowired
	private CompetitionRepository competitionRepository;

	@Autowired
	private ParticipantRepository participantRepository;

	private final Logger logger = LoggerFactory.getLogger(StartCompService.class);

	public StartCompService() {
		super(Executors.newScheduledThreadPool(4));
	}

	public boolean registerComp(long compid) {
		List<Participant> participants = this.participantRepository.findByCompid(compid);
		Optional<Competition> comp = this.competitionRepository.findById(compid);
		if (participants.size() != 0 && comp.isPresent()) {
			Skill skill = Rs3Skill.valueOf(comp.get().getSkill().toUpperCase()).getSkill();
			StartTask task = new StartTask(this.participantRepository, this, skill, compid,
					new RsHiscoresDownloader(HttpClient.HISCORES_URL));
			long delay = calculateDelay(comp.get());
			this.scheduleTask(compid, task, delay);
			return true;
		}
		return false;
	}

	public boolean rescheduleComp(long compid) {
		Optional<Competition> comp = this.competitionRepository.findById(compid);
		if (comp.isPresent()) {
			if (this.cancelTask(compid)) {
				long delay = calculateDelay(comp.get());
				Skill skill = Rs3Skill.valueOf(comp.get().getSkill().toUpperCase()).getSkill();
				StartTask task = new StartTask(this.participantRepository, this, skill, compid,
						new RsHiscoresDownloader(HttpClient.HISCORES_URL));
				this.scheduleTask(compid, task, delay);
				return true;
			}
			return false;
		}
		return false;
	}

	@Override
	public void updateCompleted(long compid) {
		logger.info("Competition [" + compid + "] has successfully started.");
		updateCompetitionState(compid, CompetitionStatus.STARTED.getStatus());
		removeCompletedTask(compid);
	}

	@Override
	public void updateStarted(long compid) {
		logger.info("Competition [" + compid + "] is starting, performing initial stat pull.");
		updateCompetitionState(compid, CompetitionStatus.UPDATING.getStatus());
	}

	private void updateCompetitionState(long compid, int status) {
		Optional<Competition> comp = this.competitionRepository.findById(compid);
		comp.ifPresent(competition -> {
			competition.setStatus(status);
			competition.setUpdatetime(Instant.now().getEpochSecond());
			this.competitionRepository.save(competition);
		});
	}

	@Override
	public void onApplicationEvent(ApplicationReadyEvent event) {
		logger.info("StartCompService creating end tasks for competitions in a NOT_STARTED state.");
		List<Competition> competitions = this.competitionRepository
				.findByStatus(CompetitionStatus.NOT_STARTED.getStatus());
		for (Competition comp : competitions) {
			Skill skill = Rs3Skill.valueOf(comp.getSkill().toUpperCase()).getSkill();
			StartTask task = new StartTask(this.participantRepository, this, skill, comp.getCompid(),
					new RsHiscoresDownloader(HttpClient.HISCORES_URL));
			long delay = calculateDelay(comp);
			if (delay < 0 && (Instant.now().getEpochSecond() - comp.getEndtime() > 0)) {
				//This started and ended before we did something, mark it done.
				logger.warn("Competition with comp id " + comp.getCompid()
						+ " was supposed to start and end prior to now, marking as finished with no stat update.");
				comp.setStatus(CompetitionStatus.FINISHED.getStatus());
				this.competitionRepository.save(comp);
			} else {
				logger.debug("Registering start task for comp id " + comp.getCompid() + " with delay of " + delay
						+ " seconds");
				this.scheduleTask(comp.getCompid(), task, delay);
			}
		}
	}

	long calculateDelay(Competition competition) {
		return competition.getStarttime() - Instant.now().getEpochSecond();
	}
}
