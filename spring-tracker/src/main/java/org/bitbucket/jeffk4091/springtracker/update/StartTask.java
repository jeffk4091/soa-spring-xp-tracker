package org.bitbucket.jeffk4091.springtracker.update;

import java.util.List;

import org.bitbucket.jeffk4091.rshiscoresfetcher.hiscoresfetcher.HiscoresRecord;
import org.bitbucket.jeffk4091.rshiscoresfetcher.hiscoresfetcher.RsHiscoresDownloader;
import org.bitbucket.jeffk4091.rshiscoresfetcher.skills.Skill;
import org.bitbucket.jeffk4091.springtracker.database.entities.Participant;
import org.bitbucket.jeffk4091.springtracker.database.repositories.ParticipantRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StartTask extends StatTask {

	private final Logger logger = LoggerFactory.getLogger(UpdateTask.class);

	public StartTask(ParticipantRepository participantRepository, StatTaskListener listener, Skill skill, long compid,
			RsHiscoresDownloader downloader) {
		super(participantRepository, listener, skill, compid, downloader);
	}

	@Override
	void updatePlayer(List<HiscoresRecord> record) {
		List<Participant> players = getParticipantRepository()
				.findByCompidAndPlayer(getCompid(), record.get(0).getUsername());
		if (players.size() == 1) {
			Participant part = players.get(0);
			HiscoresRecord applicableRecord = record.get(0);
			logger.debug("Received starting stat update for player [" + applicableRecord.getUsername()
					+ "], Competition ID [" + getCompid() + "]");
			logger.trace("Starting Stat Update: Player [" + applicableRecord.getUsername() + "], Competition ID ["
					+ getCompid() + "], XP [" + applicableRecord.getXp() + "], Level [" + applicableRecord.getLevel()
					+ "]");
			part.setStartlvl(applicableRecord.getLevel());
			part.setStartxp(applicableRecord.getXp());
			part.setEndxp(applicableRecord.getXp());
			part.setEndlvl(applicableRecord.getLevel());
			part.calculateXpAndLvlGain();
			logger.trace(
					"Saving starting stat update: Player [" + applicableRecord.getUsername() + "], Competition ID ["
							+ getCompid() + "]");
			getParticipantRepository().save(part);
		}
	}

	@Override
	void updateFailedPlayer(String name) {
		List<Participant> players = getParticipantRepository().findByCompidAndPlayer(getCompid(), name);
		if (players.size() == 1) {
			Participant part = players.get(0);
			logger.trace("Stat Update: Fetch failed for Player [" + name + "], Competition ID [" + getCompid()
					+ "], setting xp and level to 0");
			part.setEndlvl(0);
			part.setEndxp(0);
			part.setLvlgained(0);
			part.setXpgained(0);
			logger.trace("Saving stat update for Failed player [" + name + "], Competition ID [" + getCompid() + "]");
			getParticipantRepository().save(part);
		}
	}
}
