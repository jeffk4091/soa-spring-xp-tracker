package org.bitbucket.jeffk4091.springtracker.skills;

import java.util.ArrayList;
import java.util.List;

import org.bitbucket.jeffk4091.rshiscoresfetcher.skills.OldSchoolSkill;
import org.bitbucket.jeffk4091.rshiscoresfetcher.skills.Rs3Skill;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/skills")
public class SkillsController {

	@GetMapping("/")
	public List<String> getListOfSkills() {
		List<String> skills = new ArrayList<>();
		for (Rs3Skill skill : Rs3Skill.values())
			skills.add(skill.getSkillName());
		return skills;
	}

	@GetMapping("/oldschool")
	public List<String> getListOfOldSchoolSkills() {
		List<String> skills = new ArrayList<>();
		for (OldSchoolSkill skill : OldSchoolSkill.values())
			skills.add(skill.getSkillName());
		return skills;
	}
}
