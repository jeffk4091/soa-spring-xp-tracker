package org.bitbucket.jeffk4091.springtracker.update;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/update")
public class UpdateController {

	@Autowired
	private UpdateService updateService;

	@GetMapping("/")
	public boolean updateCompetition(@RequestParam("compid") long compid) {
		return updateService.updateStatsForCompetition(compid);
	}
}
