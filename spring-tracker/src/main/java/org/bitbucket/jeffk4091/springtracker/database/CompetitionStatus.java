package org.bitbucket.jeffk4091.springtracker.database;

public enum CompetitionStatus {
	NOT_STARTED(0), STARTED(1), FINISHED(2), UPDATING(3);

	private int status;

	CompetitionStatus(int status) {
		this.status = status;
	}

	public int getStatus() {
		return status;
	}
}
