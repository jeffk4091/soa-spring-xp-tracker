package org.bitbucket.jeffk4091.springtracker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class Tracker {

	public static void main(String[] args) {
		SpringApplication.run(Tracker.class);
	}
}
