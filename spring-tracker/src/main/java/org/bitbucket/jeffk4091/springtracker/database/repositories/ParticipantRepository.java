package org.bitbucket.jeffk4091.springtracker.database.repositories;

import java.util.List;

import org.bitbucket.jeffk4091.springtracker.database.entities.Participant;
import org.springframework.data.repository.CrudRepository;

public interface ParticipantRepository extends CrudRepository<Participant, Integer> {

	List<Participant> findByCompid(long compid);

	List<Participant> findByCompidAndPlayer(long compid, String player);
}
