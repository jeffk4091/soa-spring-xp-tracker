package org.bitbucket.jeffk4091.springtracker.update;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.bitbucket.jeffk4091.springtracker.database.CompetitionStatus;
import org.bitbucket.jeffk4091.springtracker.database.entities.Competition;
import org.bitbucket.jeffk4091.springtracker.database.entities.Participant;
import org.bitbucket.jeffk4091.springtracker.database.repositories.CompetitionRepository;
import org.bitbucket.jeffk4091.springtracker.database.repositories.ParticipantRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.context.event.ApplicationReadyEvent;

public class EndCompServiceTest {

	@Mock
	private CompetitionRepository competitionRepository;

	@Mock
	private ParticipantRepository participantRepository;

	@InjectMocks
	private EndCompService endCompService;

	@Before
	public void initialize() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testRegisterComp() {
		long compid = 5;

		Participant participant = new Participant();
		participant.setPlayer("Player 1");
		List<Participant> participants = new ArrayList<>();
		participants.add(participant);
		Mockito.when(participantRepository.findByCompid(compid)).thenReturn(participants);

		Competition competition = new Competition();
		competition.setSkill("Overall");
		competition.setEndtime(2000000000);
		Mockito.when(competitionRepository.findById(compid)).thenReturn(Optional.of(competition));

		ScheduledExecutorService scheduledExecutorService = Mockito.mock(ScheduledExecutorService.class);
		endCompService.setExecutorService(scheduledExecutorService);
		ScheduledFuture<?> task = Mockito.mock(ScheduledFuture.class);
		Mockito.doReturn(task).when(scheduledExecutorService).schedule(Mockito.any(Runnable.class), Mockito.anyLong(), Mockito.any(TimeUnit.class));

		Assert.assertTrue(endCompService.registerComp(5));
		Mockito.verify(scheduledExecutorService)
				.schedule(Mockito.any(UpdateTask.class), Mockito.longThat(argument -> argument > 0), Mockito.eq(TimeUnit.SECONDS));
	}

	@Test
	public void testRegisterCompFailsDueToCompDoesntExist() {
		long compid = 5;

		Participant participant = new Participant();
		participant.setPlayer("Player 1");
		List<Participant> participants = new ArrayList<>();
		participants.add(participant);
		Mockito.when(participantRepository.findByCompid(compid)).thenReturn(participants);

		Mockito.when(competitionRepository.findById(compid)).thenReturn(Optional.empty());

		Assert.assertFalse(endCompService.registerComp(5));
	}

	@Test
	public void testRescheduleComp() {
		long compid = 5;

		Participant participant = new Participant();
		participant.setPlayer("Player 1");
		List<Participant> participants = new ArrayList<>();
		participants.add(participant);
		Mockito.when(participantRepository.findByCompid(compid)).thenReturn(participants);

		//This test needs to use Mockito, as it needs to access the competition ID which does not have a setter
		Competition competition = Mockito.mock(Competition.class);
		Mockito.when(competition.getSkill()).thenReturn("Overall");
		Mockito.when(competition.getCompid()).thenReturn(5L);
		Mockito.when(competition.getEndtime()).thenReturn(2000000000L);
		Mockito.when(competitionRepository.findById(compid)).thenReturn(Optional.of(competition));

		Assert.assertTrue(endCompService.registerComp(compid));

		Mockito.when(competition.getEndtime()).thenReturn(2500000000L);

		Mockito.when(competitionRepository.findById(compid)).thenReturn(Optional.of(competition));

		Assert.assertTrue(endCompService.rescheduleComp(compid));
	}

	@Test
	public void testRescheduleCompDoesntExist() {
		long compid = 5;
		Mockito.when(competitionRepository.findById(compid)).thenReturn(Optional.empty());
		Assert.assertFalse(endCompService.rescheduleComp(compid));
	}

	@Test
	public void testRescheduleCompNotScheduled() {
		long compid = 5;

		Competition competition = new Competition();
		competition.setSkill("Overall");
		competition.setEndtime(2000000000);
		Mockito.when(competitionRepository.findById(compid)).thenReturn(Optional.of(competition));
		Assert.assertFalse(endCompService.rescheduleComp(compid));
	}

	@Test
	public void testUpdateCompleted() {
		long compid = 5;
		Competition competition = new Competition();
		competition.setSkill("Overall");
		Mockito.when(competitionRepository.findById(compid)).thenReturn(Optional.of(competition));

		endCompService.updateCompleted(compid);

		Assert.assertEquals(CompetitionStatus.FINISHED.getStatus(), competition.getStatus());
		Assert.assertTrue(competition.getUpdatetime() > 0);
	}

	@Test
	public void testUpdateStarted() {
		long compid = 5;
		Competition competition = new Competition();
		competition.setSkill("Overall");
		Mockito.when(competitionRepository.findById(compid)).thenReturn(Optional.of(competition));

		endCompService.updateStarted(compid);

		Assert.assertEquals(CompetitionStatus.UPDATING.getStatus(), competition.getStatus());
		Assert.assertTrue(competition.getUpdatetime() > 0);
	}

	@Test
	public void testCalculateDelay() {
		Competition competition = new Competition();
		competition.setSkill("Overall");
		competition.setEndtime(2000000000);

		Assert.assertTrue(endCompService.calculateDelay(competition) > 0);
	}

	@Test
	public void testNotStartedCompValidComp() {
		Competition competition = Mockito.mock(Competition.class);
		Mockito.when(competition.getSkill()).thenReturn("Overall");
		Mockito.when(competition.getEndtime()).thenReturn(20000000050L);
		Mockito.when(competition.getCompid()).thenReturn(5L);

		List<Competition> competitions = new ArrayList<>();
		competitions.add(competition);
		Mockito.when(competitionRepository.findByStatus(CompetitionStatus.NOT_STARTED.getStatus()))
				.thenReturn(competitions);
		ApplicationReadyEvent event = Mockito.mock(ApplicationReadyEvent.class);
		ScheduledExecutorService scheduledExecutorService = Mockito.mock(ScheduledExecutorService.class);
		ScheduledFuture<?> task = Mockito.mock(ScheduledFuture.class);
		Mockito.doReturn(task).when(scheduledExecutorService).schedule(Mockito.any(Runnable.class), Mockito.anyLong(), Mockito.any(TimeUnit.class));
		endCompService.setExecutorService(scheduledExecutorService);
		endCompService.onApplicationEvent(event);
		Mockito.verify(scheduledExecutorService)
				.schedule(Mockito.any(UpdateTask.class), Mockito.longThat(argument -> argument > 0),
						Mockito.eq(TimeUnit.SECONDS));
	}

	@Test
	public void testNotStartedCompThatHasAlsoEnded() {
		Competition competition = Mockito.mock(Competition.class);
		Mockito.when(competition.getSkill()).thenReturn("Overall");
		Mockito.when(competition.getEndtime()).thenReturn(150L);
		Mockito.when(competition.getCompid()).thenReturn(5L);

		List<Competition> competitions = new ArrayList<>();
		competitions.add(competition);
		Mockito.when(competitionRepository.findByStatus(CompetitionStatus.NOT_STARTED.getStatus()))
				.thenReturn(competitions);
		ApplicationReadyEvent event = Mockito.mock(ApplicationReadyEvent.class);
		ScheduledExecutorService scheduledExecutorService = Mockito.mock(ScheduledExecutorService.class);
		endCompService.setExecutorService(scheduledExecutorService);
		endCompService.onApplicationEvent(event);
		Mockito.verify(scheduledExecutorService, Mockito.never())
				.schedule(Mockito.any(UpdateTask.class), Mockito.longThat(argument -> argument <= 0),
						Mockito.eq(TimeUnit.SECONDS));
	}

	@Test
	public void testStartedComp() {
		Competition competition = Mockito.mock(Competition.class);
		Mockito.when(competition.getSkill()).thenReturn("Overall");
		Mockito.when(competition.getEndtime()).thenReturn(20000000050L);
		Mockito.when(competition.getCompid()).thenReturn(5L);

		List<Competition> competitions = new ArrayList<>();
		competitions.add(competition);
		Mockito.when(competitionRepository.findByStatus(CompetitionStatus.STARTED.getStatus()))
				.thenReturn(competitions);
		ApplicationReadyEvent event = Mockito.mock(ApplicationReadyEvent.class);
		ScheduledExecutorService scheduledExecutorService = Mockito.mock(ScheduledExecutorService.class);
		ScheduledFuture<?> task = Mockito.mock(ScheduledFuture.class);
		Mockito.doReturn(task).when(scheduledExecutorService).schedule(Mockito.any(Runnable.class), Mockito.anyLong(), Mockito.any(TimeUnit.class));
		endCompService.setExecutorService(scheduledExecutorService);
		endCompService.onApplicationEvent(event);
		Mockito.verify(scheduledExecutorService)
				.schedule(Mockito.any(UpdateTask.class), Mockito.longThat(argument -> argument > 0),
						Mockito.eq(TimeUnit.SECONDS));
	}

	@Test
	public void testUpdatingComp()
	{
		Competition competition = Mockito.mock(Competition.class);
		Mockito.when(competition.getSkill()).thenReturn("Overall");
		Mockito.when(competition.getEndtime()).thenReturn(20000000050L);
		Mockito.when(competition.getCompid()).thenReturn(5L);

		List<Competition> competitions = new ArrayList<>();
		competitions.add(competition);
		Mockito.when(competitionRepository.findByStatus(CompetitionStatus.UPDATING.getStatus()))
				.thenReturn(competitions);
		Mockito.when(competitionRepository.save(competition)).thenReturn(competition);
		ApplicationReadyEvent event = Mockito.mock(ApplicationReadyEvent.class);
		ScheduledExecutorService scheduledExecutorService = Mockito.mock(ScheduledExecutorService.class);
		endCompService.setExecutorService(scheduledExecutorService);
		ScheduledFuture<?> task = Mockito.mock(ScheduledFuture.class);
		Mockito.doReturn(task).when(scheduledExecutorService).schedule(Mockito.any(Runnable.class), Mockito.anyLong(), Mockito.any(TimeUnit.class));
		endCompService.onApplicationEvent(event);
		Mockito.verify(scheduledExecutorService)
				.schedule(Mockito.any(UpdateTask.class), Mockito.longThat(argument -> argument > 0),
						Mockito.eq(TimeUnit.SECONDS));
		Mockito.verify(competition).setStatus(CompetitionStatus.STARTED.getStatus());
	}
}
