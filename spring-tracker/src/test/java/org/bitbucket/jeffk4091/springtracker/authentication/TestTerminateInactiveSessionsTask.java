package org.bitbucket.jeffk4091.springtracker.authentication;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class TestTerminateInactiveSessionsTask {

	@Mock UUIDAuthenticationService service;

	@InjectMocks TerminateInactiveSessionsTask task;

	@Before
	public void initialize() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void verifyTask() {
		task.runTerminateInactiveSessionsTask();

		Mockito.verify(service).terminateOldSessions();
	}
}
