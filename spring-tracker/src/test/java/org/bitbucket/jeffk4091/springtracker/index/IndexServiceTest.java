package org.bitbucket.jeffk4091.springtracker.index;

import java.util.ArrayList;
import java.util.List;

import org.bitbucket.jeffk4091.springtracker.database.entities.Competition;
import org.bitbucket.jeffk4091.springtracker.database.repositories.CompetitionRepository;
import org.bitbucket.jeffk4091.springtracker.json.CompetitionDetails;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class IndexServiceTest {

	@Mock private CompetitionRepository competitionRepository;

	@InjectMocks private IndexService service;

	@Before
	public void initialize() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testGetActiveCompetitions() {
		List<Competition> competitionsList = new ArrayList<>();
		Competition competition = new Competition();
		competition.setCompname("Comp1");
		competition.setSkill("Overall");
		competition.setStarttime(1234);
		competition.setEndtime(6789);
		competition.setUpdatetime(5678);
		competition.setPrivacy(0L);
		competition.setStatus(1);
		competitionsList.add(competition);

		competition = new Competition();
		competition.setCompname("Comp2");
		competition.setSkill("Overall");
		competition.setStarttime(1234);
		competition.setEndtime(6789);
		competition.setUpdatetime(5678);
		competition.setPrivacy(0L);
		competition.setStatus(3);
		competitionsList.add(competition);

		Mockito.when(
				competitionRepository.findByStatusInAndPrivacyInOrderByCompidDesc(Mockito.anyList(), Mockito.anyList()))
				.thenReturn(competitionsList);

		List<CompetitionDetails> competitionDetails = service.getActiveCompetitions(true);

		Assert.assertEquals(2, competitionDetails.size());
		Assert.assertEquals(1, competitionDetails.get(0).getStatus());
		Assert.assertEquals(3, competitionDetails.get(1).getStatus());

	}

	@Test
	public void testGetUpcomingCompetitions() {
		List<Competition> competitionsList = new ArrayList<>();
		Competition competition = new Competition();
		competition.setCompname("Comp1");
		competition.setSkill("Overall");
		competition.setStarttime(1234);
		competition.setEndtime(6789);
		competition.setUpdatetime(5678);
		competition.setPrivacy(0L);
		competition.setStatus(0);
		competitionsList.add(competition);

		Mockito.when(
				competitionRepository.findByStatusAndPrivacyInOrderByStarttimeAsc(Mockito.anyInt(), Mockito.anyList()))
				.thenReturn(competitionsList);

		List<CompetitionDetails> competitionDetails = service.getUpcomingCompetitions(true);

		Assert.assertEquals(1, competitionDetails.size());
		Assert.assertEquals(0, competitionDetails.get(0).getStatus());
	}

	@Test
	public void testGetPreviousCompetitionsTop10() {
		List<Competition> competitionsList = new ArrayList<>();
		for (int i = 0; i < 10; i++) {
			Competition competition = new Competition();
			competition.setCompname("Comp " + i);
			competition.setSkill("Overall");
			competition.setStarttime(1234);
			competition.setEndtime(6789);
			competition.setUpdatetime(5678);
			competition.setPrivacy(0L);
			competition.setStatus(2);
			competitionsList.add(competition);
		}

		Mockito.when(competitionRepository
				.findTop10ByStatusAndPrivacyInOrderByEndtimeDesc(Mockito.anyInt(), Mockito.anyList()))
				.thenReturn(competitionsList);

		List<CompetitionDetails> competitionDetails = service.getPreviousCompetitions(true, false);

		Assert.assertEquals(10, competitionDetails.size());
		Assert.assertEquals(2, competitionDetails.get(0).getStatus());
	}

	@Test
	public void testGetPreviousCompetitions() {
		List<Competition> competitionsList = new ArrayList<>();
		for (int i = 0; i < 15; i++) {
			Competition competition = new Competition();
			competition.setCompname("Comp " + i);
			competition.setSkill("Overall");
			competition.setStarttime(1234);
			competition.setEndtime(6789);
			competition.setUpdatetime(5678);
			competition.setPrivacy(0L);
			competition.setStatus(2);
			competitionsList.add(competition);
		}

		Mockito.when(
				competitionRepository.findByStatusAndPrivacyInOrderByEndtimeDesc(Mockito.anyInt(), Mockito.anyList()))
				.thenReturn(competitionsList);

		List<CompetitionDetails> competitionDetails = service.getPreviousCompetitions(true, true);

		Assert.assertEquals(15, competitionDetails.size());
		Assert.assertEquals(2, competitionDetails.get(0).getStatus());
	}

	@Test
	public void testGetActiveNoPrivate() {
		service.getActiveCompetitions(false);

		Mockito.verify(competitionRepository)
				.findByStatusInAndPrivacyInOrderByCompidDesc(Mockito.anyList(), Mockito.anyList());
	}

	@Test
	public void testGetUpcomingCompetitionsNoPrivate() {
		service.getUpcomingCompetitions(false);

		Mockito.verify(competitionRepository)
				.findByStatusAndPrivacyInOrderByStarttimeAsc(Mockito.anyInt(), Mockito.anyList());
	}

	@Test
	public void testGetPreviousNoPrivate() {
		service.getPreviousCompetitions(false, true);

		Mockito.verify(competitionRepository)
				.findByStatusAndPrivacyInOrderByEndtimeDesc(Mockito.anyInt(), Mockito.anyList());
	}

}
