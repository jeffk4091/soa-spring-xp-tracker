package org.bitbucket.jeffk4091.springtracker.update;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutorService;

import org.bitbucket.jeffk4091.springtracker.database.CompetitionStatus;
import org.bitbucket.jeffk4091.springtracker.database.entities.Competition;
import org.bitbucket.jeffk4091.springtracker.database.entities.Participant;
import org.bitbucket.jeffk4091.springtracker.database.repositories.CompetitionRepository;
import org.bitbucket.jeffk4091.springtracker.database.repositories.ParticipantRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class UpdateServiceTest {

	@Mock
	private CompetitionRepository competitionRepository;

	@Mock
	private ParticipantRepository participantRepository;

	@InjectMocks
	private UpdateService updateService;

	@Before
	public void initialize() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testUpdateStatsForCompetition() {
		ExecutorService service = Mockito.mock(ExecutorService.class);
		updateService.setExecutorService(service);
		long compid = 5;

		Participant participant = new Participant();
		participant.setPlayer("Player 1");
		List<Participant> participants = new ArrayList<>();
		participants.add(participant);
		Mockito.when(participantRepository.findByCompid(compid)).thenReturn(participants);

		Competition competition = new Competition();
		competition.setSkill("Overall");
		competition.setStatus(CompetitionStatus.STARTED.getStatus());
		competition.setEndtime(20000000000L);
		Mockito.when(competitionRepository.findById(compid)).thenReturn(Optional.of(competition));

		Assert.assertTrue(updateService.updateStatsForCompetition(compid));

		Mockito.verify(service).submit(Mockito.any(Runnable.class));
	}

	@Test
	public void testUpdateStatsForEndedCompetition()
	{
		ExecutorService service = Mockito.mock(ExecutorService.class);
		updateService.setExecutorService(service);
		long compid = 5;

		Participant participant = new Participant();
		participant.setPlayer("Player 1");
		List<Participant> participants = new ArrayList<>();
		participants.add(participant);
		Mockito.when(participantRepository.findByCompid(compid)).thenReturn(participants);

		Competition competition = new Competition();
		competition.setSkill("Overall");
		competition.setStatus(CompetitionStatus.FINISHED.getStatus());
		competition.setEndtime(20000000000L);
		Mockito.when(competitionRepository.findById(compid)).thenReturn(Optional.of(competition));

		Assert.assertFalse(updateService.updateStatsForCompetition(compid));

		Mockito.verify(service, Mockito.never()).submit(Mockito.any(Runnable.class));
	}

	@Test
	public void testUpdateSTatsForCompThatShouldHaveEnded()
	{
		ExecutorService service = Mockito.mock(ExecutorService.class);
		updateService.setExecutorService(service);
		long compid = 5;

		Participant participant = new Participant();
		participant.setPlayer("Player 1");
		List<Participant> participants = new ArrayList<>();
		participants.add(participant);
		Mockito.when(participantRepository.findByCompid(compid)).thenReturn(participants);

		Competition competition = new Competition();
		competition.setSkill("Overall");
		competition.setStatus(CompetitionStatus.STARTED.getStatus());
		competition.setEndtime(150);
		Mockito.when(competitionRepository.findById(compid)).thenReturn(Optional.of(competition));

		Assert.assertFalse(updateService.updateStatsForCompetition(compid));

		Mockito.verify(service, Mockito.never()).submit(Mockito.any(Runnable.class));
	}

	@Test
	public void testUpdateStatsForCompetitionWithNoParticipants() {
		long compid = 5;
		List<Participant> participants = new ArrayList<>();
		Mockito.when(participantRepository.findByCompid(compid)).thenReturn(participants);
		Competition competition = new Competition();
		competition.setSkill("Overall");
		Mockito.when(competitionRepository.findById(compid)).thenReturn(Optional.of(competition));

		Assert.assertFalse(updateService.updateStatsForCompetition(compid));
	}

	@Test
	public void testUpdateCompetitionState() {
		long compid = 5;
		Competition competition = new Competition();
		competition.setSkill("Overall");
		Mockito.when(competitionRepository.findById(compid)).thenReturn(Optional.of(competition));

		updateService.updateCompetitionState(compid, CompetitionStatus.UPDATING);

		Assert.assertEquals(CompetitionStatus.UPDATING.getStatus(), competition.getStatus());
	}

	@Test
	public void testUpdateCompleted() {
		long compid = 5;
		Competition competition = new Competition();
		competition.setSkill("Overall");
		Mockito.when(competitionRepository.findById(compid)).thenReturn(Optional.of(competition));

		updateService.updateCompleted(compid);

		Assert.assertEquals(CompetitionStatus.STARTED.getStatus(), competition.getStatus());
		Assert.assertTrue(competition.getUpdatetime() > 0);
	}

	@Test
	public void testUpdateStarted() {
		long compid = 5;
		Competition competition = new Competition();
		competition.setSkill("Overall");
		Mockito.when(competitionRepository.findById(compid)).thenReturn(Optional.of(competition));

		updateService.updateStarted(compid);

		Assert.assertEquals(CompetitionStatus.UPDATING.getStatus(), competition.getStatus());
		Assert.assertTrue(competition.getUpdatetime() > 0);
	}
}
