package org.bitbucket.jeffk4091.springtracker.update;

import java.util.ArrayList;
import java.util.List;

import org.bitbucket.jeffk4091.rshiscoresfetcher.hiscoresfetcher.HiscoresRecord;
import org.bitbucket.jeffk4091.rshiscoresfetcher.hiscoresfetcher.RsHiscoresDownloader;
import org.bitbucket.jeffk4091.rshiscoresfetcher.skills.Rs3Skill;
import org.bitbucket.jeffk4091.rshiscoresfetcher.skills.Skill;
import org.bitbucket.jeffk4091.springtracker.database.entities.Participant;
import org.bitbucket.jeffk4091.springtracker.database.repositories.ParticipantRepository;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

public class StatTaskTest {

	@Test
	public void testConstructor() {
		ParticipantRepository repo = Mockito.mock(ParticipantRepository.class);
		RsHiscoresDownloader downloader = Mockito.mock(RsHiscoresDownloader.class);
		StatTaskListener listener = Mockito.mock(StatTaskListener.class);
		Skill skill = Rs3Skill.OVERALL.getSkill();
		long compid = 5;

		StatTask statTask = new StatTask(repo, listener, skill, compid, downloader) {
			@Override
			void updatePlayer(List<HiscoresRecord> record) {

			}

			@Override
			void updateFailedPlayer(String name) {

			}
		};

		Assert.assertEquals(skill, statTask.getSkill().get(0));
		Mockito.verify(downloader).addListener(statTask);
		Assert.assertEquals(repo, statTask.getParticipantRepository());
		Assert.assertEquals(compid, statTask.getCompid());
	}

	@Test
	public void testRun() {
		ParticipantRepository repo = Mockito.mock(ParticipantRepository.class);
		RsHiscoresDownloader downloader = Mockito.mock(RsHiscoresDownloader.class);
		List<String> participants = new ArrayList<>();
		participants.add("Player 1");

		Participant participant = new Participant();
		participant.setPlayer("Player 1");
		List<Participant> players = new ArrayList<>();
		players.add(participant);
		long compid = 5;
		Mockito.when(repo.findByCompid(compid)).thenReturn(players);
		StatTaskListener listener = Mockito.mock(StatTaskListener.class);
		Skill skill = Rs3Skill.OVERALL.getSkill();

		StatTask statTask = new StatTask(repo, listener, skill, compid, downloader) {
			@Override
			void updatePlayer(List<HiscoresRecord> record) {

			}

			@Override
			void updateFailedPlayer(String name) {

			}
		};

		statTask.run();
		Mockito.verify(downloader).listenForStatsOfPlayers(participants, statTask.getSkill());
		Mockito.verify(listener).updateCompleted(compid);
	}

	@Test
	public void testNotifySuccessfulFetch() throws Exception {
		RsHiscoresDownloader downloader = Mockito.mock(RsHiscoresDownloader.class);
		StatTaskListener listener = Mockito.mock(StatTaskListener.class);
		Skill skill = Rs3Skill.OVERALL.getSkill();
		long compid = 5;

		final boolean[] taskRun = { false };

		StatTask statTask = new StatTask(null, listener, skill, compid, downloader) {
			@Override
			void updatePlayer(List<HiscoresRecord> record) {
				taskRun[0] = true;
			}

			@Override
			void updateFailedPlayer(String name) {

			}
		};

		List<HiscoresRecord> record = new ArrayList<>();

		statTask.notifySuccessfulFetch(record);
		Thread.sleep(100);
		Assert.assertTrue(taskRun[0]);

	}

	@Test
	public void testNotifyFailedFetch() throws Exception {
		RsHiscoresDownloader downloader = Mockito.mock(RsHiscoresDownloader.class);
		StatTaskListener listener = Mockito.mock(StatTaskListener.class);
		Skill skill = Rs3Skill.OVERALL.getSkill();
		long compid = 5;

		final boolean[] taskRun = { false };

		StatTask statTask = new StatTask(null, listener, skill, compid, downloader) {
			@Override
			void updatePlayer(List<HiscoresRecord> record) {
			}

			@Override
			void updateFailedPlayer(String name) {
				taskRun[0] = true;
			}
		};

		statTask.notifyFailedFetch("Player 1");
		Thread.sleep(100);
		Assert.assertTrue(taskRun[0]);

	}

}
