package org.bitbucket.jeffk4091.springtracker.update;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

public class StatTaskSchedulerTest {

	@Test
	public void testScheduleTask()
	{
		StatTask statTask = Mockito.mock(StatTask.class);
		ScheduledExecutorService service = Mockito.mock(ScheduledExecutorService.class);
		ScheduledFuture future = Mockito.mock(ScheduledFuture.class);
		Mockito.when(service.schedule(statTask, 50, TimeUnit.SECONDS)).thenReturn(future);
		StatTaskScheduler scheduler = new StatTaskScheduler(service);

		scheduler.scheduleTask(5, statTask, 50);
		Mockito.verify(service).schedule(statTask, 50, TimeUnit.SECONDS);
		Assert.assertNotNull(scheduler.getFutureForCompid(5));
	}

	@Test
	public void testCancelTaskSuccess()
	{
		StatTask statTask = Mockito.mock(StatTask.class);
		ScheduledExecutorService service = Mockito.mock(ScheduledExecutorService.class);
		ScheduledFuture future = Mockito.mock(ScheduledFuture.class);
		Mockito.when(future.cancel(false)).thenReturn(true);
		Mockito.when(service.schedule(statTask, 50, TimeUnit.SECONDS)).thenReturn(future);
		StatTaskScheduler scheduler = new StatTaskScheduler(service);

		scheduler.scheduleTask(5, statTask, 50);
		Assert.assertTrue(scheduler.cancelTask(5));
	}

	@Test
	public void testCancelTestFailureAsDoesntExist()
	{
		ScheduledExecutorService service = Mockito.mock(ScheduledExecutorService.class);
		StatTaskScheduler scheduler = new StatTaskScheduler(service);

		Assert.assertFalse(scheduler.cancelTask(5));
	}

	@Test
	public void testIsCompScheduled()
	{
		StatTask statTask = Mockito.mock(StatTask.class);
		ScheduledExecutorService service = Mockito.mock(ScheduledExecutorService.class);
		ScheduledFuture future = Mockito.mock(ScheduledFuture.class);
		Mockito.when(service.schedule(statTask, 50, TimeUnit.SECONDS)).thenReturn(future);
		StatTaskScheduler scheduler = new StatTaskScheduler(service);

		scheduler.scheduleTask(5, statTask, 50);
		Assert.assertTrue(scheduler.isCompScheduled(5));
	}
}
