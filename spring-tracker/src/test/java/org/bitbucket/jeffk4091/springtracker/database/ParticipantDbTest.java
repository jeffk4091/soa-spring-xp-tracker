package org.bitbucket.jeffk4091.springtracker.database;

import java.util.List;

import org.bitbucket.jeffk4091.springtracker.database.entities.Participant;
import org.bitbucket.jeffk4091.springtracker.database.repositories.ParticipantRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.security.core.parameters.P;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ParticipantDbTest {

	@Autowired
	private ParticipantRepository participantRepository;

	@Autowired
	private TestEntityManager entityManager;

	@Test
	public void testGetParticipantsByCompid()
	{
		Participant participant = new Participant();
		participant.setCompid(5);
		participant.setPlayer("Player 1");
		participant.setStartxp(5000);
		participant.setEndxp(5000);
		participant.setStartlvl(50);
		participant.setEndlvl(50);
		participant.setLvlgained(0);
		participant.setXpgained(0);

		entityManager.persist(participant);
		entityManager.flush();

		participant = new Participant();
		participant.setCompid(6);
		participant.setPlayer("Player 3");
		participant.setStartxp(5000);
		participant.setEndxp(5000);
		participant.setStartlvl(50);
		participant.setEndlvl(50);
		participant.setLvlgained(0);
		participant.setXpgained(0);

		entityManager.persist(participant);
		entityManager.flush();

		List<Participant> participants = participantRepository.findByCompid(5);
		Assert.assertEquals(1, participants.size());
	}

	@Test
	public void testFindByCompidAndPlayer()
	{
		Participant participant = new Participant();
		participant.setCompid(5);
		participant.setPlayer("Player 1");
		participant.setStartxp(5000);
		participant.setEndxp(5000);
		participant.setStartlvl(50);
		participant.setEndlvl(50);
		participant.setLvlgained(0);
		participant.setXpgained(0);

		entityManager.persist(participant);
		entityManager.flush();

		participant = new Participant();
		participant.setCompid(6);
		participant.setPlayer("Player 3");
		participant.setStartxp(5000);
		participant.setEndxp(5000);
		participant.setStartlvl(50);
		participant.setEndlvl(50);
		participant.setLvlgained(0);
		participant.setXpgained(0);

		entityManager.persist(participant);
		entityManager.flush();

		List<Participant> participants = participantRepository.findByCompidAndPlayer(5, "Player 1");
		Assert.assertEquals(1, participants.size());
	}

}
