package org.bitbucket.jeffk4091.springtracker.skills;

import org.bitbucket.jeffk4091.springtracker.authentication.TokenAuthenticationProvider;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@RunWith(SpringRunner.class)
@WebMvcTest(SkillsController.class)
public class SkillsControllerTest {

	@Autowired private MockMvc mockMvc;

	@MockBean private TokenAuthenticationProvider provider;

	@Test
	public void testSkillsOutput() throws Exception {
		MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.get("/skills/")).andReturn();
		String output = mvcResult.getResponse().getContentAsString();

		output = output.replace("[", "").replace("]", "");
		String[] outputArray = output.split(",");
		Assert.assertEquals(29, outputArray.length);
		Assert.assertEquals("\"Overall\"", outputArray[0]);
		Assert.assertEquals("\"Archaeology\"", outputArray[outputArray.length - 1]);
	}

	@Test
	public void testOldSchoolSkillsOutput() throws Exception {
		MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.get("/skills/oldschool")).andReturn();
		String output = mvcResult.getResponse().getContentAsString();

		output = output.replace("[", "").replace("]", "");
		String[] outputArray = output.split(",");
		Assert.assertEquals(24, outputArray.length);
		Assert.assertEquals("\"Overall\"", outputArray[0]);
		Assert.assertEquals("\"Construction\"", outputArray[outputArray.length - 1]);
	}
}
