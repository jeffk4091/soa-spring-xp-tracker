package org.bitbucket.jeffk4091.springtracker.index;

import java.util.ArrayList;
import java.util.List;

import org.bitbucket.jeffk4091.springtracker.authentication.TokenAuthenticationProvider;
import org.bitbucket.jeffk4091.springtracker.json.CompetitionDetails;
import org.bitbucket.jeffk4091.springtracker.util.DateConverter;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@RunWith(SpringRunner.class)
@WebMvcTest(IndexController.class)
public class IndexControllerTest {

	@Autowired private MockMvc mockMvc;

	@MockBean private IndexService indexService;

	@MockBean private TokenAuthenticationProvider provider;

	private List<CompetitionDetails> createCompetition(int status, long privacy) {
		List<CompetitionDetails> competitionDetailsList = new ArrayList<>();
		DateConverter converter = new DateConverter();
		CompetitionDetails competitionDetails = new CompetitionDetails(1, "Test Competition", "Overall", status,
				converter.formatEpochToString(1575329754), converter.formatEpochToString(1575329804),
				converter.formatEpochToString(1575329774), privacy);
		competitionDetailsList.add(competitionDetails);
		return competitionDetailsList;
	}

	@Test
	public void testGetActiveCompetitions() throws Exception {
		Mockito.when(indexService.getActiveCompetitions(true)).thenReturn(createCompetition(1, 0));

		MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.get("/index/active")).andReturn();

		Assert.assertEquals(200, mvcResult.getResponse().getStatus());
		Assert.assertTrue(mvcResult.getResponse().getContentAsString().contains("Test Competition"));
		Mockito.verify(indexService).getActiveCompetitions(Mockito.anyBoolean());
	}

	@Test
	public void testGetUpcomingCompetitions() throws Exception {
		Mockito.when(indexService.getUpcomingCompetitions(true)).thenReturn(createCompetition(0, 0));

		MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.get("/index/upcoming")).andReturn();

		Assert.assertEquals(200, mvcResult.getResponse().getStatus());
		Assert.assertTrue(mvcResult.getResponse().getContentAsString().contains("Test Competition"));
		Mockito.verify(indexService).getUpcomingCompetitions(Mockito.anyBoolean());
	}

	@Test
	public void testGetPreviousCompetitions() throws Exception {
		Mockito.when(indexService.getPreviousCompetitions(true, false)).thenReturn(createCompetition(2, 0));

		MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.get("/index/previous")).andReturn();

		Assert.assertEquals(200, mvcResult.getResponse().getStatus());
		Assert.assertTrue(mvcResult.getResponse().getContentAsString().contains("Test Competition"));
		Mockito.verify(indexService).getPreviousCompetitions(true, false);
	}

	@Test
	public void testGetPreviousCompetitionsViewAll() throws Exception {
		Mockito.when(indexService.getPreviousCompetitions(true, true)).thenReturn(createCompetition(2, 0));

		MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.get("/index/previous?viewall")).andReturn();

		Assert.assertEquals(200, mvcResult.getResponse().getStatus());
		Assert.assertTrue(mvcResult.getResponse().getContentAsString().contains("Test Competition"));
		Mockito.verify(indexService).getPreviousCompetitions(true, true);
	}

}
