package org.bitbucket.jeffk4091.springtracker.util;

import java.text.ParseException;

import org.junit.Assert;
import org.junit.Test;

public class DateConverterTest {

	@Test
	public void convertEpochSecondToDate() {
		DateConverter converter = new DateConverter();
		Assert.assertEquals("2019/11/29 16:00", converter.formatEpochToString(1575043200));
	}

	@Test
	public void convertDateToEpochSecond() throws ParseException {
		String date = "2019/11/29 16:00";
		DateConverter converter = new DateConverter();
		Assert.assertEquals(1575043200, converter.formatDateToEpochSecond(date));
	}
}
