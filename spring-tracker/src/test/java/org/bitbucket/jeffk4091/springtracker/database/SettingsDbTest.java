package org.bitbucket.jeffk4091.springtracker.database;

import org.bitbucket.jeffk4091.springtracker.database.entities.Setting;
import org.bitbucket.jeffk4091.springtracker.database.repositories.SettingsRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class SettingsDbTest {

	@Autowired private SettingsRepository settingsRepository;

	@Autowired private TestEntityManager entityManager;

	@Test
	public void testGetSettingBySetName() {
		Setting setting = new Setting();
		setting.setSetname("adminpass");
		setting.setSetval("adminpass-val");

		entityManager.persist(setting);
		entityManager.flush();

		Setting finalSetting = settingsRepository.findBySetname("adminpass");
		Assert.assertEquals("adminpass-val", finalSetting.getSetval());
	}

}
