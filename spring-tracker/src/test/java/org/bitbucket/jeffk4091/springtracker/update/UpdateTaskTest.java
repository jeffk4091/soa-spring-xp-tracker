package org.bitbucket.jeffk4091.springtracker.update;

import java.util.ArrayList;
import java.util.List;

import org.bitbucket.jeffk4091.rshiscoresfetcher.hiscoresfetcher.HiscoresRecord;
import org.bitbucket.jeffk4091.rshiscoresfetcher.hiscoresfetcher.RsHiscoresDownloader;
import org.bitbucket.jeffk4091.rshiscoresfetcher.skills.Rs3Skill;
import org.bitbucket.jeffk4091.springtracker.database.entities.Participant;
import org.bitbucket.jeffk4091.springtracker.database.repositories.ParticipantRepository;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

public class UpdateTaskTest {

	@Test
	public void testUpdatePlayer() {
		ParticipantRepository repository = Mockito.mock(ParticipantRepository.class);
		RsHiscoresDownloader downloader = Mockito.mock(RsHiscoresDownloader.class);
		List<String> participants = new ArrayList<>();
		participants.add("Player 1");
		long compid = 5;

		Participant participant = new Participant();
		participant.setPlayer("Player 1");
		participant.setStartxp(5000);
		participant.setStartlvl(20);
		participant.setEndxp(5000);
		participant.setEndlvl(5000);

		List<Participant> participantRepoList = new ArrayList<>();
		participantRepoList.add(participant);

		Mockito.when(repository.findByCompidAndPlayer(compid, "Player 1")).thenReturn(participantRepoList);

		HiscoresRecord record = new HiscoresRecord("Player 1", Rs3Skill.OVERALL.getSkill(), 10000, 35, 1024);
		List<HiscoresRecord> records = new ArrayList<>();
		records.add(record);

		UpdateTask updateTask = new UpdateTask(repository, null, null, compid, downloader);

		updateTask.updatePlayer(records);

		Assert.assertEquals(10000, participant.getEndxp());
		Assert.assertEquals(35, participant.getEndlvl());
		Assert.assertEquals(5000, participant.getXpgained());
		Assert.assertEquals(15, participant.getLvlgained());

		Mockito.verify(repository).save(participant);
	}

	@Test
	public void testUpdatePlayerStartLvlAndXpAreZero() {
		ParticipantRepository repository = Mockito.mock(ParticipantRepository.class);
		RsHiscoresDownloader downloader = Mockito.mock(RsHiscoresDownloader.class);
		List<String> participants = new ArrayList<>();
		participants.add("Player 1");
		long compid = 5;

		Participant participant = new Participant();
		participant.setPlayer("Player 1");
		participant.setStartxp(0);
		participant.setStartlvl(0);
		participant.setEndxp(0);
		participant.setEndlvl(0);

		List<Participant> participantRepoList = new ArrayList<>();
		participantRepoList.add(participant);

		Mockito.when(repository.findByCompidAndPlayer(compid, "Player 1")).thenReturn(participantRepoList);

		HiscoresRecord record = new HiscoresRecord("Player 1", Rs3Skill.OVERALL.getSkill(), 10000, 35, 1024);
		List<HiscoresRecord> records = new ArrayList<>();
		records.add(record);

		UpdateTask updateTask = new UpdateTask(repository, null, null, compid, downloader);

		updateTask.updatePlayer(records);

		Assert.assertEquals(10000, participant.getStartxp());
		Assert.assertEquals(35, participant.getStartlvl());
		Assert.assertEquals(10000, participant.getEndxp());
		Assert.assertEquals(35, participant.getEndlvl());
		Assert.assertEquals(0, participant.getXpgained());
		Assert.assertEquals(0, participant.getLvlgained());

		Mockito.verify(repository).save(participant);

	}

	@Test
	public void updateNonExistantPlayer() {
		ParticipantRepository repository = Mockito.mock(ParticipantRepository.class);
		RsHiscoresDownloader downloader = Mockito.mock(RsHiscoresDownloader.class);
		List<String> participants = new ArrayList<>();
		participants.add("Player 1");
		long compid = 5;

		Mockito.when(repository.findByCompidAndPlayer(compid, "Player 1")).thenReturn(new ArrayList<Participant>());

		HiscoresRecord record = new HiscoresRecord("Player 1", Rs3Skill.OVERALL.getSkill(), 10000, 35, 1024);
		List<HiscoresRecord> records = new ArrayList<>();
		records.add(record);

		UpdateTask updateTask = new UpdateTask(repository, null, null, compid, downloader);

		updateTask.updatePlayer(records);

		Mockito.verify(repository, Mockito.never()).save(Mockito.any(Participant.class));
	}

	@Test
	public void testFailedPlayer() {
		ParticipantRepository repository = Mockito.mock(ParticipantRepository.class);
		RsHiscoresDownloader downloader = Mockito.mock(RsHiscoresDownloader.class);
		List<String> participants = new ArrayList<>();
		participants.add("Player 1");
		long compid = 5;

		Participant participant = new Participant();
		participant.setPlayer("Player 1");
		participant.setStartxp(5000);
		participant.setStartlvl(20);
		participant.setEndxp(5000);
		participant.setEndlvl(5000);

		List<Participant> participantRepoList = new ArrayList<>();
		participantRepoList.add(participant);

		Mockito.when(repository.findByCompidAndPlayer(compid, "Player 1")).thenReturn(participantRepoList);

		UpdateTask updateTask = new UpdateTask(repository, null, null, compid, downloader);
		updateTask.updateFailedPlayer("Player 1");

		Assert.assertEquals(0, participant.getEndxp());
		Assert.assertEquals(0, participant.getEndlvl());
		Assert.assertEquals(0, participant.getXpgained());
		Assert.assertEquals(0, participant.getLvlgained());

		Mockito.verify(repository).save(participant);
	}

	@Test
	public void testFailedPlayerWhoDoesntExist() {
		ParticipantRepository repository = Mockito.mock(ParticipantRepository.class);
		RsHiscoresDownloader downloader = Mockito.mock(RsHiscoresDownloader.class);
		List<String> participants = new ArrayList<>();
		participants.add("Player 1");
		long compid = 5;

		Participant participant = new Participant();
		participant.setPlayer("Player 1");
		participant.setStartxp(5000);
		participant.setStartlvl(20);
		participant.setEndxp(5000);
		participant.setEndlvl(5000);

		List<Participant> participantRepoList = new ArrayList<>();
		participantRepoList.add(participant);

		Mockito.when(repository.findByCompidAndPlayer(compid, "Player 1")).thenReturn(participantRepoList);

		UpdateTask updateTask = new UpdateTask(repository, null, null, compid, downloader);
		updateTask.updateFailedPlayer("Player 2");

		Mockito.verify(repository, Mockito.never()).save(Mockito.any(Participant.class));
	}

}
