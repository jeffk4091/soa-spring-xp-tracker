package org.bitbucket.jeffk4091.springtracker.database;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.bitbucket.jeffk4091.springtracker.database.entities.Competition;
import org.bitbucket.jeffk4091.springtracker.database.repositories.CompetitionRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CompetitionsDbTest {

	/*
	 * Note - While Spring will roll back the DB contents after each test, it will NOT reset the auto-increment
	 * counter.  Write tests accordingly.
	 */

	@Autowired private CompetitionRepository competitionRepository;

	@Autowired private TestEntityManager entityManager;

	@Test
	public void findCompetitionsById() {
		Competition competition = new Competition();
		competition.setCompname("Test Competition 1");
		competition.setPrivacy(1);
		competition.setStarttime(123456);
		competition.setEndtime(654321);
		competition.setStatus(1);
		competition.setUpdatetime(345612);
		competition.setSkill("Overall");

		Competition firstComp = entityManager.persist(competition);
		entityManager.flush();

		competition = new Competition();
		competition.setCompname("Test Competition 2");
		competition.setPrivacy(1);
		competition.setStarttime(123456);
		competition.setEndtime(654321);
		competition.setStatus(1);
		competition.setUpdatetime(345612);
		competition.setSkill("Overall");

		Competition secondComp = entityManager.persist(competition);
		entityManager.flush();

		Optional<Competition> resultComp = competitionRepository.findById(firstComp.getCompid());
		Assert.assertTrue(resultComp.isPresent());
		Assert.assertEquals("Test Competition 1", resultComp.get().getCompname());

		Optional<Competition> resultComp2 = competitionRepository.findById(secondComp.getCompid());
		Assert.assertTrue(resultComp2.isPresent());
		Assert.assertEquals("Test Competition 2", resultComp2.get().getCompname());

	}

	@Test
	public void findByStatus() {
		Competition competition = new Competition();
		competition.setCompname("Test Competition 1");
		competition.setPrivacy(1);
		competition.setStarttime(123456);
		competition.setEndtime(654321);
		competition.setStatus(2);
		competition.setUpdatetime(345612);
		competition.setSkill("Overall");

		entityManager.persist(competition);
		entityManager.flush();

		competition = new Competition();
		competition.setCompname("Test Competition 2");
		competition.setPrivacy(1);
		competition.setStarttime(123456);
		competition.setEndtime(654321);
		competition.setStatus(1);
		competition.setUpdatetime(345612);
		competition.setSkill("Overall");

		entityManager.persist(competition);
		entityManager.flush();

		List<Competition> resultComp = competitionRepository.findByStatus(2);
		Assert.assertEquals(1, resultComp.size());
		Assert.assertEquals("Test Competition 1", resultComp.get(0).getCompname());
	}

	@Test
	public void testFindByStatusInAndPrivacyInOrderByCompidDesc() {
		Competition competition = new Competition();
		competition.setCompname("Test Competition 1");
		competition.setPrivacy(0);
		competition.setStarttime(123456);
		competition.setEndtime(654321);
		competition.setStatus(1);
		competition.setUpdatetime(345612);
		competition.setSkill("Overall");

		Competition firstComp = entityManager.persist(competition);
		entityManager.flush();

		competition = new Competition();
		competition.setCompname("Test Competition 2");
		competition.setPrivacy(1);
		competition.setStarttime(123456);
		competition.setEndtime(654321);
		competition.setStatus(3);
		competition.setUpdatetime(345612);
		competition.setSkill("Overall");

		Competition secondComp = entityManager.persist(competition);
		entityManager.flush();

		List<Integer> statuses = new ArrayList<>();
		statuses.add(1);
		statuses.add(3);
		List<Long> privacy = new ArrayList<>();
		privacy.add(0L);
		privacy.add(1L);

		List<Competition> finalComps = competitionRepository
				.findByStatusInAndPrivacyInOrderByCompidDesc(statuses, privacy);
		Assert.assertEquals(2, finalComps.size());
		Assert.assertEquals(secondComp.getCompid(), finalComps.get(0).getCompid());
		Assert.assertEquals(secondComp.getStatus(), finalComps.get(0).getStatus());
		Assert.assertEquals(secondComp.getPrivacy(), finalComps.get(0).getPrivacy());
		Assert.assertEquals(firstComp.getCompid(), finalComps.get(1).getCompid());
		Assert.assertEquals(firstComp.getStatus(), finalComps.get(1).getStatus());
		Assert.assertEquals(firstComp.getPrivacy(), finalComps.get(1).getPrivacy());
	}

	@Test
	public void testFindByStatusAndPrivacyOrderByStarttimeAsc() {
		Competition competition = new Competition();
		competition.setCompname("Test Competition 1");
		competition.setPrivacy(0);
		competition.setStarttime(123456);
		competition.setEndtime(654321);
		competition.setStatus(0);
		competition.setUpdatetime(345612);
		competition.setSkill("Overall");

		entityManager.persist(competition);
		entityManager.flush();

		competition = new Competition();
		competition.setCompname("Test Competition 2");
		competition.setPrivacy(1);
		competition.setStarttime(678910);
		competition.setEndtime(654321);
		competition.setStatus(0);
		competition.setUpdatetime(345612);
		competition.setSkill("Overall");

		entityManager.persist(competition);
		entityManager.flush();

		List<Long> privacy = new ArrayList<>();
		privacy.add(0L);
		privacy.add(1L);

		List<Competition> finalComps = competitionRepository.findByStatusAndPrivacyInOrderByStarttimeAsc(0, privacy);
		Assert.assertEquals(2, finalComps.size());
		Assert.assertEquals("Test Competition 1", finalComps.get(0).getCompname());
		Assert.assertEquals("Test Competition 2", finalComps.get(1).getCompname());
		Assert.assertEquals(0, finalComps.get(0).getPrivacy());
		Assert.assertEquals(1, finalComps.get(1).getPrivacy());
	}

	@Test
	public void testFindTop10ByStatusAndPrivacyOrderByEndtime() {
		for (int i = 0; i < 15; i++) {
			Competition competition = new Competition();
			competition.setCompname("Test Competition " + i);
			competition.setPrivacy(0);
			competition.setStarttime(123456);
			competition.setEndtime(654321 + i);
			competition.setStatus(2);
			competition.setUpdatetime(345612);
			competition.setSkill("Overall");

			entityManager.persist(competition);
			entityManager.flush();
		}
		Competition competition = new Competition();
		competition.setCompname("Test Competition 15");
		competition.setPrivacy(1);
		competition.setStarttime(123456);
		competition.setEndtime(987654);
		competition.setStatus(2);
		competition.setUpdatetime(345612);
		competition.setSkill("Overall");

		entityManager.persist(competition);
		entityManager.flush();
		List<Long> privacy = new ArrayList<>();
		privacy.add(0L);
		privacy.add(1L);
		List<Competition> resultComp = competitionRepository
				.findTop10ByStatusAndPrivacyInOrderByEndtimeDesc(2, privacy);

		Assert.assertEquals(10, resultComp.size());
		Assert.assertEquals("Test Competition 15", resultComp.get(0).getCompname());
		Assert.assertEquals(1, resultComp.get(0).getPrivacy());
		Assert.assertEquals("Test Competition 6", resultComp.get(9).getCompname());
		Assert.assertEquals(0, resultComp.get(9).getPrivacy());

	}

	@Test
	public void testFindByStatusAndPrivacyOrderByEndtime() {
		for (int i = 0; i < 15; i++) {
			Competition competition = new Competition();
			competition.setCompname("Test Competition " + i);
			competition.setPrivacy(0);
			competition.setStarttime(123456);
			competition.setEndtime(654321 + i);
			competition.setStatus(2);
			competition.setUpdatetime(345612);
			competition.setSkill("Overall");

			entityManager.persist(competition);
			entityManager.flush();
		}
		Competition competition = new Competition();
		competition.setCompname("Test Competition 15");
		competition.setPrivacy(1);
		competition.setStarttime(123456);
		competition.setEndtime(987654);
		competition.setStatus(2);
		competition.setUpdatetime(345612);
		competition.setSkill("Overall");

		entityManager.persist(competition);
		entityManager.flush();
		List<Long> privacy = new ArrayList<>();
		privacy.add(0L);
		privacy.add(1L);
		List<Competition> resultComp = competitionRepository.findByStatusAndPrivacyInOrderByEndtimeDesc(2, privacy);

		Assert.assertEquals(16, resultComp.size());
		Assert.assertEquals("Test Competition 15", resultComp.get(0).getCompname());
		Assert.assertEquals(1, resultComp.get(0).getPrivacy());
		Assert.assertEquals("Test Competition 6", resultComp.get(9).getCompname());
		Assert.assertEquals(0, resultComp.get(9).getPrivacy());

	}

}
