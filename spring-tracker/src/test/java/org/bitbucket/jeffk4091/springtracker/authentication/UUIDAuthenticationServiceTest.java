package org.bitbucket.jeffk4091.springtracker.authentication;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Pattern;

import org.bitbucket.jeffk4091.springtracker.database.entities.Setting;
import org.bitbucket.jeffk4091.springtracker.database.repositories.SettingsRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class UUIDAuthenticationServiceTest {

	@Mock private SettingsRepository settingsRepository;

	@Mock private ConcurrentHashMap<String, User> loggedInUsersMap;

	@InjectMocks private UUIDAuthenticationService authenticationService;

	private Pattern uuidPattern = Pattern
			.compile("([a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}){1}");
	private static final String HELLO_ENCRYPTED = "$2y$12$PzwzdmD0DF8.3iaq8GYL/.OWIRJiNvgxwbG6Hm3W8nhTpiqS3jB6S";

	@Before
	public void initialize() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testLogin() {
		Setting setting = new Setting();
		setting.setSetname("adminpass");
		setting.setSetval(HELLO_ENCRYPTED);
		Mockito.when(settingsRepository.findBySetname("adminpass")).thenReturn(setting);

		Optional<String> loginUuid = authenticationService.login("hello");

		Assert.assertTrue(loginUuid.isPresent());
		Assert.assertTrue(uuidPattern.matcher(loginUuid.get()).matches());
		Mockito.verify(loggedInUsersMap).put(Mockito.eq(loginUuid.get()), Mockito.any(User.class));
	}

	@Test
	public void testLoginFail() {
		Setting setting = new Setting();
		setting.setSetname("adminpass");
		setting.setSetval(HELLO_ENCRYPTED);
		Mockito.when(settingsRepository.findBySetname("adminpass")).thenReturn(setting);

		Optional<String> loginUuid = authenticationService.login("failedLogin");

		Assert.assertFalse(loginUuid.isPresent());
		Mockito.verifyNoInteractions(loggedInUsersMap);
	}

	@Test
	public void testFindByToken() {
		String uuid = UUID.randomUUID().toString();
		User user = new User("hello", uuid);
		Mockito.when(loggedInUsersMap.get(uuid)).thenReturn(user);
		Assert.assertEquals(Optional.of(user), authenticationService.findByToken(uuid));
	}

	@Test
	public void testFindByTokenNotPresent() {
		String uuid = UUID.randomUUID().toString();
		Mockito.when(loggedInUsersMap.get(uuid)).thenReturn(null);
		Assert.assertEquals(Optional.empty(), authenticationService.findByToken(uuid));
	}

	@Test
	public void testLogout() {
		String uuid = UUID.randomUUID().toString();
		User user = new User("hello", uuid);
		List<User> users = new ArrayList<>();
		users.add(user);
		Mockito.when(loggedInUsersMap.values()).thenReturn(users);
		authenticationService.logout(user);

		Mockito.verify(loggedInUsersMap).remove(uuid);
	}

	@Test
	public void testUserExpired() {
		String uuid = UUID.randomUUID().toString();
		User user = new User("hello", uuid);
		user.setLastActive(Instant.now().minus(26, ChronoUnit.MINUTES));
		String uuid2 = UUID.randomUUID().toString();
		User user2 = new User("hello", uuid2);
		user2.setLastActive(Instant.now().minus(24, ChronoUnit.MINUTES));
		List<User> users = new ArrayList<>();
		users.add(user);
		users.add(user2);
		Mockito.when(loggedInUsersMap.values()).thenReturn(users);
		authenticationService.terminateOldSessions();

		Mockito.verify(loggedInUsersMap).remove(uuid);
		Mockito.verify(loggedInUsersMap, Mockito.never()).remove(uuid2);

	}
}
