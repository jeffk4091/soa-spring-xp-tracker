package org.bitbucket.jeffk4091.springtracker.update;

import org.bitbucket.jeffk4091.springtracker.authentication.TokenAuthenticationProvider;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@RunWith(SpringRunner.class)
@WebMvcTest(UpdateController.class)
public class UpdateControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private TokenAuthenticationProvider provider;

	@MockBean
	private UpdateService updateService;

	@Test
	public void testUpdateCompetition() throws Exception {
		Mockito.when(updateService.updateStatsForCompetition(5L)).thenReturn(true);

		MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.get("/update/?compid=5")).andReturn();

		Assert.assertEquals(200, mvcResult.getResponse().getStatus());
		Assert.assertEquals("true", mvcResult.getResponse().getContentAsString());
		Mockito.verify(updateService).updateStatsForCompetition(5);
	}
}
