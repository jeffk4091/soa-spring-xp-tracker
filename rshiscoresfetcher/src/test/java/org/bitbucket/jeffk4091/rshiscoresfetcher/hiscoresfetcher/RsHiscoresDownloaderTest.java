package org.bitbucket.jeffk4091.rshiscoresfetcher.hiscoresfetcher;

import java.util.ArrayList;
import java.util.List;

import org.bitbucket.jeffk4091.rshiscoresfetcher.skills.Rs3Skill;
import org.bitbucket.jeffk4091.rshiscoresfetcher.skills.Skill;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;

/**
 * Note - As this class is meant to be a Unit test only, it uses a Mock web server and does not
 * actually contact Jagex's Hiscores API.
 * For testing with the actual API, use the RsHiscoresDownloaderApp instead.
 */
public class RsHiscoresDownloaderTest {

	@Test
	public void testDownloadStats() throws Exception {
		MockWebServer server = new MockWebServer();
		server.enqueue(new MockResponse().setBody(appleStats));
		server.start();
		RsHiscoresDownloader downloader = new RsHiscoresDownloader(server.url("/lookup=").toString());
		String data = null;
		try {
			data = downloader.downloadHiscores("Applejuiceaj");
		} catch (Exception e) {
			Assert.fail();
		}
		server.shutdown();
		Assert.assertEquals(appleStats, data);

	}

	@Test
	public void testUserDoesNotExist() throws Exception {
		MockWebServer server = new MockWebServer();
		server.enqueue(new MockResponse().setResponseCode(404));
		server.start();
		RsHiscoresDownloader downloader = new RsHiscoresDownloader(server.url("/lookup=").toString());
		String data = null;
		try {
			data = downloader.downloadHiscores("Mod Meadows");
			Assert.fail();
		} catch (Exception e) {
		}
		RecordedRequest request1 = server.takeRequest();
		Assert.assertEquals("/lookup=Mod%20Meadows", request1.getPath());
		server.shutdown();
	}

	@Test
	public void testParseValidStats() {
		RsHiscoresDownloader downloader = new RsHiscoresDownloader(
				"https://secure.runescape.com/m=hiscore/index_lite.ws?player=");
		HiscoresRecord record = null;
		try {
			record = downloader.parseData("Applejuiceaj", Rs3Skill.OVERALL.getSkill(), appleStats);
		} catch (Exception e) {
			Assert.fail();
		}

		Assert.assertEquals(record.getUsername(), "Applejuiceaj");
		Assert.assertEquals(record.getSkill().getSkillName(), "Overall");
		Assert.assertEquals(record.getLevel(), 2751);
		Assert.assertEquals(record.getXp(), 1178212885);
		Assert.assertEquals(record.getRank(), 23229);

	}

	@Test
	public void testParseInvalidStats() {
		RsHiscoresDownloader downloader = new RsHiscoresDownloader(
				"https://secure.runescape.com/m=hiscore/index_lite.ws?player=");
		try {
			downloader.parseData("Invalid Player", Rs3Skill.OVERALL.getSkill(), "miscellaneous junk");
			Assert.fail();
		} catch (Exception e) {
			//Success if it makes it here
		}
	}

	@Test
	public void testParseStatsNotRanked() {
		RsHiscoresDownloader downloader = new RsHiscoresDownloader(
				"https://secure.runescape.com/m=hiscore/index_lite.ws?player=");
		HiscoresRecord record = null;
		try {
			record = downloader.parseData("AppleTheElf", Rs3Skill.ATTACK.getSkill(), ateStats);
		} catch (Exception e) {
			Assert.fail();
		}

		Assert.assertEquals(record.getUsername(), "AppleTheElf");
		Assert.assertEquals(record.getSkill().getSkillName(), Rs3Skill.ATTACK.getSkillName());
		Assert.assertEquals(record.getXp(), -1);
		Assert.assertEquals(record.getLevel(), 1);
		Assert.assertEquals(-1, record.getRank());
	}

	@Test
	public void testParseStatsNotParseable() {
		RsHiscoresDownloader downloader = new RsHiscoresDownloader(
				"https://secure.runescape.com/m=hiscore/index_lite.ws?player=");
		try {
			downloader.parseData("Applejuicean", Rs3Skill.OVERALL.getSkill(), "-1,finally,done");
			Assert.fail();
		} catch (Exception e) {
			//Success if it makes it here.
		}
	}

	@Test
	public void testFetchAndParseValidStats() throws Exception {
		MockWebServer server = new MockWebServer();
		server.enqueue(new MockResponse().setBody(appleStats));
		server.start();
		RsHiscoresDownloader downloader = new RsHiscoresDownloader(server.url("/lookup=").toString());
		List<Skill> skills = new ArrayList<>();
		skills.add(Rs3Skill.DUNGEONEERING.getSkill());
		List<HiscoresRecord> record = new ArrayList<>();
		try {
			record = downloader.getStatsForSinglePlayer("Applejuiceaj", skills);
		} catch (Exception e) {
			Assert.fail();
		}

		Assert.assertEquals(record.get(0).getUsername(), "Applejuiceaj");
		Assert.assertEquals(record.get(0).getSkill().getSkillName(), Rs3Skill.DUNGEONEERING.getSkillName());
		Assert.assertEquals(record.get(0).getLevel(), 120);
		Assert.assertEquals(record.get(0).getXp(), 200000000);
		Assert.assertEquals(6396, record.get(0).getRank());
		server.shutdown();

	}

	@Test
	public void testFetchAndParseMultipleValidStats() throws Exception {
		MockWebServer server = new MockWebServer();
		server.enqueue(new MockResponse().setBody(appleStats));
		server.enqueue(new MockResponse().setBody(ateStats));
		server.enqueue(new MockResponse().setResponseCode(404));
		server.start();
		RsHiscoresDownloader downloader = new RsHiscoresDownloader(server.url("/lookup=").toString());

		List<HiscoresRecord> record = null;
		List<String> names = new ArrayList<>();
		names.add("Applejuiceaj");
		names.add("AppleTheElf");
		names.add("Mod Meadows");
		List<Skill> skills = new ArrayList<>();
		skills.add(Rs3Skill.OVERALL.getSkill());

		try {
			record = downloader.getStatsForListOfPlayers(names, skills);
		} catch (Exception e) {
			Assert.fail();
		}

		server.shutdown();

		Assert.assertEquals(record.size(), 2);
		HiscoresRecord record1 = record.get(0);
		Assert.assertEquals(record1.getUsername(), "Applejuiceaj");
		Assert.assertEquals(record1.getLevel(), 2751);

		HiscoresRecord record2 = record.get(1);
		Assert.assertEquals(record2.getUsername(), "AppleTheElf");

	}

	@Test
	public void listenForStats() throws Exception {
		MockWebServer server = new MockWebServer();
		server.enqueue(new MockResponse().setBody(appleStats));
		server.enqueue(new MockResponse().setBody(ateStats));
		server.enqueue(new MockResponse().setResponseCode(404));
		server.start();
		RsHiscoresDownloader downloader = new RsHiscoresDownloader(server.url("/lookup=").toString());

		List<String> names = new ArrayList<>();
		names.add("Applejuiceaj");
		names.add("AppleTheElf");
		names.add("Mod Meadows");
		RsHiscoresDownloaderListener listener = Mockito.mock(RsHiscoresDownloaderListener.class);
		downloader.addListener(listener);
		List<Skill> skills = new ArrayList<>();
		skills.add(Rs3Skill.OVERALL.getSkill());
		downloader.listenForStatsOfPlayers(names, skills);

		server.shutdown();

		Mockito.verify(listener, Mockito.times(2)).notifySuccessfulFetch(Mockito.anyList());
		Mockito.verify(listener, Mockito.times(1)).notifyFailedFetch(Mockito.anyString());

	}

	private static final String appleStats =
			"23229,2751,1178212885\n" + "126630,99,15408975\n" + "137508,99,15879089\n" + "125145,99,15910909\n"
					+ "128972,99,26824437\n" + "78689,99,26201447\n" + "58346,99,16064543\n" + "80737,99,25039896\n"
					+ "21236,99,28043227\n" + "8544,99,55887368\n" + "16918,99,23983320\n" + "6380,99,113748341\n"
					+ "22297,99,37455560\n" + "9345,99,43390203\n" + "22568,99,27541094\n" + "22870,99,45825680\n"
					+ "52671,102,17885347\n" + "9403,99,46183402\n" + "41264,99,27973200\n" + "48657,111,45895118\n"
					+ "10408,120,105362628\n" + "28104,99,17966690\n" + "15689,99,33337287\n" + "12113,99,30440786\n"
					+ "28837,99,21086065\n" + "6396,120,200000000\n" + "13848,99,32790088\n" + "55122,120,82088185\n"
					+ "-1,-1\n" + "-1,-1\n" + "-1,-1\n" + "-1,-1\n" + "-1,-1\n" + "-1,-1\n" + "-1,-1\n" + "60803,850\n"
					+ "-1,-1\n" + "-1,-1\n" + "-1,-1\n" + "-1,-1\n" + "43051,1937\n" + "2855,680\n" + "3751,617\n"
					+ "12984,1305200\n" + "-1,-1\n" + "32926,2\n" + "-1,-1\n" + "-1,-1\n" + "-1,-1\n" + "-1,-1\n"
					+ "-1,-1\n" + "-1,-1\n" + "42590,10560\n" + "136528,1\n" + "130511,1\n" + "62595,48\n"
					+ "43813,57\n" + "36536,6\n";

	private static final String ateStats =
			"751589,902,3888525\n" + "-1,1,-1\n" + "-1,1,-1\n" + "-1,1,-1\n" + "-1,1,-1\n" + "-1,1,-1\n" + "-1,1,-1\n"
					+ "-1,1,-1\n" + "501926,68,625438\n" + "604728,68,625076\n" + "645795,48,86699\n"
					+ "497755,67,549364\n" + "493412,68,640523\n" + "551917,57,217844\n" + "569381,58,234561\n"
					+ "747808,52,131235\n" + "539170,42,47416\n" + "614635,40,38319\n" + "720266,27,9749\n"
					+ "766189,17,3494\n" + "405434,51,115988\n" + "489050,52,134799\n" + "420397,57,208318\n"
					+ "426057,55,175627\n" + "-1,1,-1\n" + "759748,17,3407\n" + "409520,40,39510\n" + "-1,0,-1\n"
					+ "-1,-1\n" + "-1,-1\n" + "-1,-1\n" + "-1,-1\n" + "-1,-1\n" + "-1,-1\n" + "-1,-1\n" + "-1,-1\n"
					+ "-1,-1\n" + "-1,-1\n" + "-1,-1\n" + "-1,-1\n" + "-1,-1\n" + "-1,-1\n" + "-1,-1\n"
					+ "58690,107455\n" + "-1,-1\n" + "-1,-1\n" + "-1,-1\n" + "-1,-1\n" + "-1,-1\n" + "-1,-1\n"
					+ "-1,-1\n" + "-1,-1\n" + "634229,760\n" + "-1,-1\n" + "-1,-1\n" + "-1,-1\n" + "-1,-1\n"
					+ "-1,-1\n";

}
