package org.bitbucket.jeffk4091.rshiscoresfetcher.hiscoresfetcher;

import java.util.ArrayList;
import java.util.List;

import org.bitbucket.jeffk4091.rshiscoresfetcher.skills.Rs3Skill;
import org.bitbucket.jeffk4091.rshiscoresfetcher.skills.Skill;
import org.bitbucket.jeffk4091.rshiscoresfetcher.util.HttpClient;

public class RsHiscoresDownloaderApp {

	public static void main(String[] args) throws Exception {
		RsHiscoresDownloader downloader = new RsHiscoresDownloader(HttpClient.HISCORES_URL);
		List<Skill> skills = new ArrayList<>();
		skills.add(Rs3Skill.OVERALL.getSkill());
		List<HiscoresRecord> record = downloader.getStatsForSinglePlayer("Applejuiceaj", skills);

		System.out.println("Player: " + record.get(0).getUsername());
		System.out.println("Skill: " + record.get(0).getSkill().getSkillName());
		System.out.println("Level: " + record.get(0).getLevel());
		System.out.println("Experience: " + record.get(0).getXp());
	}
}
