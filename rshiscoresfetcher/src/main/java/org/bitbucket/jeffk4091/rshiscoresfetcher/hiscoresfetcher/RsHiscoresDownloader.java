package org.bitbucket.jeffk4091.rshiscoresfetcher.hiscoresfetcher;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.bitbucket.jeffk4091.rshiscoresfetcher.skills.Skill;
import org.bitbucket.jeffk4091.rshiscoresfetcher.util.HttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import okhttp3.Request;
import okhttp3.Response;

public class RsHiscoresDownloader {

	private final Logger logger = LoggerFactory.getLogger(RsHiscoresDownloader.class);
	private String lookupUrl;
	private RsHiscoresDownloaderListener listener;

	public void addListener(RsHiscoresDownloaderListener listener) {
		this.listener = listener;
	}

	public RsHiscoresDownloader(String lookupUrl) {
		this.lookupUrl = lookupUrl;
	}

	public List<HiscoresRecord> getStatsForSinglePlayer(String playerName, List<Skill> skill) throws Exception {
		String data = downloadHiscores(playerName);
		List<HiscoresRecord> records = new ArrayList<>();
		for (Skill sk : skill) {
			records.add(parseData(playerName, sk, data));
		}
		return records;
	}

	public List<HiscoresRecord> getStatsForListOfPlayers(List<String> playerNames, List<Skill> skill) {
		List<HiscoresRecord> records = new ArrayList<>();
		for (String player : playerNames) {
			try {
				records.addAll(getStatsForSinglePlayer(player, skill));
			} catch (Exception e) {
				logger.error("Error occurred when downloading stats: " + e.getMessage());
				//Skipping...
			}
		}
		return records;
	}

	public void listenForStatsOfPlayers(List<String> playerNames, List<Skill> skill) {
		for (String player : playerNames) {
			try {
				List<HiscoresRecord> records = getStatsForSinglePlayer(player, skill);
				listener.notifySuccessfulFetch(records);
			} catch (Exception e) {
				logger.error("Error occurred when downloading stats: " + e.getMessage());
				listener.notifyFailedFetch(player);
			}
		}
	}

	public String downloadHiscores(String playerName) throws Exception {
		URL url = new URL(lookupUrl + playerName);

		Request request = new Request.Builder().url(url).build();
		String responseBody;
		try (Response response = HttpClient.CLIENT.newCall(request).execute()) {
			if (response.isSuccessful())
				responseBody = Objects.requireNonNull(response.body()).string();
			else {
				throw new Exception("Did not receive a response of 200 when fetching stats for [" + playerName
						+ "], response was instead " + response.code());
			}
		}
		return responseBody;
	}

	public HiscoresRecord parseData(String playerName, Skill skill, String data) throws Exception {
		//Tried System.lineSeparator but Jagex separates on \n, so force that.
		String[] lines = data.split("\n");
		String skillLine = lines[skill.getSkillPosition() - 1];
		String[] entry = skillLine.split(",");
		if (entry.length == 3) {
			long xp = Long.parseLong(entry[2]);
			int level = Integer.parseInt(entry[1]);
			long rank = Long.parseLong(entry[0]);
			return new HiscoresRecord(playerName, skill, xp, level, rank);
		} else {
			throw new Exception("Received malformed data from the Hiscores API");
		}
	}

}
