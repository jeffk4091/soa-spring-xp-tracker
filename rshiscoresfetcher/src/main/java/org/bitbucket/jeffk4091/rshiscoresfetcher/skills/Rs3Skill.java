package org.bitbucket.jeffk4091.rshiscoresfetcher.skills;

public enum Rs3Skill {
	OVERALL("Overall", 1),
	ATTACK("Attack", 2),
	DEFENCE("Defence", 3),
	STRENGTH("Strength", 4),
	CONSTITUTION("Constitution", 5),
	RANGED("Ranged", 6),
	PRAYER("Prayer", 7),
	MAGIC("Magic", 8),
	COOKING("Cooking", 9),
	WOODCUTTING("Woodcutting", 10),
	FLETCHING("Fletching", 11),
	FISHING("Fishing", 12),
	FIREMAKING("Firemaking", 13),
	CRAFTING("Crafting", 14),
	SMITHING("Smithing", 15),
	MINING("Mining", 16),
	HERBLORE("Herblore", 17),
	AGILITY("Agility", 18),
	THIEVING("Thieving", 19),
	SLAYER("Slayer", 20),
	FARMING("Farming", 21),
	RUNECRAFTING("Runecrafting", 22),
	HUNTER("Hunter", 23),
	CONSTRUCTION("Construction", 24),
	SUMMONING("Summoning", 25),
	DUNGEONEERING("Dungoneering", 26),
	DIVINATION("Divination", 27),
	INVENTION("Invention", 28),
	ARCHAEOLOGY("Archaeology", 29);

	private String skillName;
	private int position;

	Rs3Skill(String skillName, int position) {
		this.skillName = skillName;
		this.position = position;
	}

	public String getSkillName() {
		return skillName;
	}

	public int getPosition() {
		return position;
	}

	public Skill getSkill() {
		return new Skill(skillName, position);
	}
}
