package org.bitbucket.jeffk4091.rshiscoresfetcher.skills;

public class Skill {

	private String skillName;
	private int skillPosition;

	public Skill(String skillName, int skillPosition) {
		this.skillName = skillName;
		this.skillPosition = skillPosition;
	}

	public String getSkillName() {
		return skillName;
	}

	public int getSkillPosition() {
		return skillPosition;
	}
}
