package org.bitbucket.jeffk4091.rshiscoresfetcher.hiscoresfetcher;

import java.util.List;

public interface RsHiscoresDownloaderListener {

	void notifySuccessfulFetch(List<HiscoresRecord> record);

	void notifyFailedFetch(String name);
}
