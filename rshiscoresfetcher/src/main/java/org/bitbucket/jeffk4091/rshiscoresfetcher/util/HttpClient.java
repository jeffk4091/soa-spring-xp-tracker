package org.bitbucket.jeffk4091.rshiscoresfetcher.util;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;

public class HttpClient {

	public static final OkHttpClient CLIENT = new OkHttpClient().newBuilder().connectTimeout(30, TimeUnit.SECONDS)
			.readTimeout(30, TimeUnit.SECONDS).build();
	public static final String HISCORES_URL = "https://secure.runescape.com/m=hiscore/index_lite.ws?player=";

}
