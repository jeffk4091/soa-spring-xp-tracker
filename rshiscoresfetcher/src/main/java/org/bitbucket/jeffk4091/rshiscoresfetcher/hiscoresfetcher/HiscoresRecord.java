package org.bitbucket.jeffk4091.rshiscoresfetcher.hiscoresfetcher;

import org.bitbucket.jeffk4091.rshiscoresfetcher.skills.Skill;

public class HiscoresRecord {

	private String username;
	private Skill skill;
	private long xp;
	private int level;
	private long rank;

	public HiscoresRecord(String username, Skill skill, long xp, int level, long rank) {
		this.username = username;
		this.skill = skill;
		this.xp = xp;
		this.level = level;
		this.rank = rank;
	}

	public String getUsername() {
		return username;
	}

	public Skill getSkill() {
		return skill;
	}

	public long getXp() {
		return xp;
	}

	public int getLevel() {
		return level;
	}

	public long getRank() {
		return rank;
	}
}
